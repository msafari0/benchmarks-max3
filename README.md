# Benchmarks of MaX applications

This repository contains the benchmark results of MaX3 applications, organized to facilitate easy analysis and comparison across workloads, platforms, and versions.

## How to contribute

Benchmarks data should be uploaded under the following structure

`<application>/<workload>/<platform>/<version>_<year>-<month>`

The benchmark folder should contain

* result.dat : benchmark results for selected application clocks ;
* inputfiles : the folder with inputs for benchmark workload ;
* logfiles   : the JUBE output with the application logfiles and xml scripts.

where JUBE output has the following stucture

```
000000 # main directory
|
├── 000001_submit # workpackage directory
│   ├── done     
│   └── work
│       ├── < benchmark_inputs >
│       ├── < benchmark_outputs >
│       ├── < slurm logfiles >
│       ├── stderr # jube stderr
│       ├── stdout # jube stdout
│       └── submit.job # jobscript for submission
├── analyse.log # logfiles for analyse step (jube )
├── analyse.xml # output of analysis step (jube )
├── configuration.xml # configuration file for the benchmark
├── continue.log # logfiles for continue command (jube )
├── parse.log # logfile for parsing step (jube )
├── result # folder with result table
│   └── result.dat
├── result.log # logfile for result step (jube)
├── run.log # logfile for run step (jube )
├── timestamps
└── workpackages.xml # concretization of the different workpackages (jube )
```

## Visualization of Benchmark Data

The web interface aim to visualized data is integrated into a CI/CD pipeline. Please note that **each commit titled with `[triggerci]`** will automatically trigger a regeneration of the HTML plots (`git commit -m [triggerci]`).

The benchmark data (visualized through a web interface) available via [Gitlab pages](https://benchmarks-max3-max-centre-9958bb5003607b47e1a11ae4c1cc8cae2cd0.gitlab.io/).

For detailed instructions on using the visualization interface, including how to navigate and analyze results, refer to the [Visualisation instructions](https://gitlab.com/max-centre/benchmarks-max3/-/blob/master/.visualisation/README.md?ref_type=heads).


