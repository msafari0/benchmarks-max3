# BigDFT Benchmarks for High Performance Computing

This repository contains a collection of benchmarks for the **BigDFT** software suite in the context of High Performance Computing (HPC).
These benchmarks are designed to assess the performance of the BigDFT code on various supercomputing architectures and configurations.

As a test of our remote run capabilities, this project runs some benchmarks
of molecular crystals. It benchmarks both the cubic and linear scaling version
of the code. Since we pass systems and logfiles back and forth, this is also
a good test of our serialization capabilities.

The benchmarks positions are in the `input` directory. There are two benchmark notebooks
you can run. Before running, you need to create the specifications for
the computer you are using. To do that, you should create a description of the remote manchine in terms of a yaml specification file.

An exemple of this specification file can be found in the page [Remote Compilation of BigDFT](https://l_sim.gitlab.io/bigdft-suite/users/RemoteCompilation.html)

BigDFT is a DFT (Density Functional Theory) code based on a wavelet formalism, which can efficiently scale to large systems and high-performance architectures.
For more details about BigDFT, please visit the [official BigDFT website](https://bigdft.org).

## Table of Contents
1. [Prerequisites](#prerequisites)
2. [Compiling BigDFT](#compiling-bigdft)
3. [Running the Benchmarks](#running-the-benchmarks)
4. [Benchmark Results](#benchmark-results)


## Prerequisites

Before running the benchmarks, ensure that you have access to an HPC system and have the necessary libraries and dependencies installed. You will need:

- A supported compiler (GCC, Intel, etc.)
- MPI (Message Passing Interface) for parallel execution
- Math libraries (BLAS, LAPACK, etc.)
- Python for post-processing (optional but recommended)

### Python Packages

In addition to the system dependencies, the local machine where you manage or post-process the benchmarks should have the following Python packages installed:


- [`remotemanager`](https://l_sim.gitlab.io/remotemanager): Facilitates remote job submission and management across HPC systems.
- [`pyfutile`](https://l_sim.gitlab.io/futile/pyfutile.html): Provides utility functions and tools for data handling.
- [`pybigdft`](https://l_sim.gitlab.io/bigdft-suite/PyBigDFT/build/html/index.html): The Python bindings for BigDFT, which allow for script-based control of BigDFT workflows.

You can install these packages using `pip` (once again, this installation should be performed *locally*):

 pip install remotemanager pyfutile pybigdft

Please refer to your HPC's documentation for setting up the required environment for compiling and running parallel applications.

## Compiling BigDFT
To compile the BigDFT suite on your target HPC system, follow the detailed instructions provided on the BigDFT official documentation page:

[BigDFT Source Installation Guide](https://l_sim.gitlab.io/bigdft-suite/users/source-install.html)

These instructions will guide you through the steps to configure, build, and install BigDFT on your system. Be sure to configure the installation according to your specific hardware and software environment (e.g., using the appropriate MPI compiler wrappers, and optimized math libraries).

### Example Steps
 Here there is a exemple about how to compile the code

    git clone https://gitlab.com/l_sim/bigdft-suite.git
    mkdir build && cd build
    ../bigdft-suite/Installer.py -c "<configureline>"

Replace the configure line with the appropriate options for your system.

For further details, refer to the installation guide linked above.

## Running the Benchmarks
Once BigDFT is successfully compiled, you can proceed to run the benchmarks included in this repository. The benchmarks are organized into various folders, each corresponding to a specific system size and setup.

### Folder Structure
    benchmark/
     ├── BenchmarkAnalysis.ipynb
     ├── csvfiles
     └── datadirectories/

Each folder contains:

  * BenchmarkAnalysis.ipynb: the notebook which contains th eanalysis of the benchmark.

  * datadirectories : the folders which are releated to the results of the Benchmarks, which are created by the remote runner calculations.

  * csvfiles: the crinched data analyzed by the jupyter notebook

 ## Running a Benchmark
   Each benchmark has been created in the  same way as explained in the Notebook `Benchmark-Prepare.ipynb`

   To be noticed: such a notebook _prepare_ and _submit_ the benchmark on the remote machine as per indicated by the `remotemanager` package.
   Such notebook will take care of the submission, execution and data retrieval of the remote bench.


 ## Benchmark Results

  The Benchmark results are then stored in a local directory on the user's machine and can be processed
  as indicated in the `BenchmarkAnalysis.ipynb` notebook of the corresponding directory.

# Want to know more ?

 The entire datasets of this prvject can be found in the [remote-bench repo](https://gitlab.com/wddawson/bigdft-remote-bench) of BigDFT group.
 Have a hear at the Google’s NotebookLM podcast about remotemanager in the `.wav` file of that repository.
 And of course, you can find further instructions in [remotemanager](https://l_sim.gitlab.io/remotemanager) and [bigdft](https://bigdft.org) documentations.
