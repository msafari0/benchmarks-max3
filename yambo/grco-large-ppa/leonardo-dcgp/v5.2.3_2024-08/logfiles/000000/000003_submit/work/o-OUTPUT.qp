#                                                                     
#  **    **    **     ****     ****  ******     *******               
# //**  **    ****   /**/**   **/** /*////**   **/////**              
#  //****    **//**  /**//** ** /** /*   /**  **     //**             
#   //**    **  //** /** //***  /** /******  /**      /**             
#    /**   **********/**  //*   /** /*//// **/**      /**             
#    /**  /**//////**/**   /    /** /*    /**//**     **              
#    /**  /**     /**/**        /** /*******  //*******               
#    //   //      // //         //  ///////    ///////                
#                                                                     
# Version 5.2.3 Revision 22799 Hash (prev commit) bad66dc080          
#                        Branch is                                    
#            MPI+OpenMP+SLK+HDF5_MPI_IO Build                         
#                http://www.yambo-code.eu                             
#
#
# GW solver                                       : Newton
# GW approximation                                : PPA
# PPA imaginary Energy                            :   27.2113838    [ev]
# RIM G`s                                         :  1
# RIM random pts                                  :  3000000
# dS/dw steps                                     :  2
# dS/dw step                                      :  0.100000001    [ev]
# X G`s                                           :  1953 [used]
# X G`s                                           :  1953 [disk]
# X bands                                         :     1  2000
# X poles                                         :   100.000000    [o/o]
# X e/h E range                                   :  -1.00000000     -1.00000000    [ev]
# X Hxc Kernel                                    : HARTREE
# X BZ energy Double Grid                         : no
# X Optical Averaged directions                   : none
# Sc/G bands                                      :     1  2000
# Sc/G damping                                    :  0.100000001    [ev]
# Sc bands terminator                             : no
# Sx RL components                                :  110539
#                                                 
# QP @ state[ 1 ] K range                         :  1  1
# QP @ state[ 1 ] b range                         :   38   39
# QP @ state[ 2 ] K range                         :  1  1
# QP @ state[ 2 ] b range                         :   44   45
# QP @ state[ 3 ] K range                         :   61   61
# QP @ state[ 3 ] b range                         :   35   36
# QP @ state[ 4 ] K range                         :   61   61
# QP @ state[ 4 ] b range                         :   40   41
# GF energies kind                                : Perdew, Burke & Ernzerhof(X)+Perdew, Burke & Ernzerhof(C)
# GF WF`s kind                                    : Perdew, Burke & Ernzerhof(X)+Perdew, Burke & Ernzerhof(C)
# Xs energies kind                                : Perdew, Burke & Ernzerhof(X)+Perdew, Burke & Ernzerhof(C)
# Xs WF`s kind                                    : Perdew, Burke & Ernzerhof(X)+Perdew, Burke & Ernzerhof(C)
#
# Vxc  =Perdew, Burke & Ernzerhof(X)+Perdew, Burke & Ernzerhof(C)
# Vnlxc=Hartree-Fock
#
#    K-point            Band               Eo [eV]            E-Eo [eV]          Sc|Eo [eV]         Spin_Pol
#
        1                   38               -1.740995           0.108488           2.792455           1
        1                   38               -0.140447           0.665081           2.725194          -1
        1                   39               -1.368899           1.425702           12.29269           1
        1                   39                0.263202           0.601508          -1.293200          -1
        1                   44               -0.838163           0.986325           9.870298           1
        1                   44                1.007234          -0.050116          -4.933268          -1
        1                   45                2.333668          -1.911435          -7.190761           1
        1                   45                2.454685          -2.080551          -7.459488          -1
         61                 35               -1.816791           1.558862           12.04084           1
         61                 35               -0.251765           1.302467           3.251397          -1
         61                 36               -1.254039           1.517882           9.595887           1
         61                 36                0.308174           0.398362          -2.654414          -1
         61                 40               -0.157885           1.010157           5.074553           1
         61                 40                1.075654           0.163306          -4.310659          -1
         61                 41                0.260181          -0.446669          -1.582528           1
         61                 41                1.443974          -0.626226          -7.702031          -1
# 
# 08/23/2024 at 18:56 yambo @ lrdn3542.leonardo.local [start]
# 08/24/2024 at 01:54                                 [end]
#  
# Timing   [Min/Max/Average]: 06h-57m/06h-57m/06h-57m
#
# .-Input file  yambo-grco-ppa.in
# | HF_and_locXC                     # [R] Hartree-Fock
# | gw0                              # [R] GW approximation
# | rim_cut                          # [R] Coulomb potential
# | em1d                             # [R][X] Dynamically Screened Interaction
# | ppa                              # [R][Xp] Plasmon Pole Approximation for the Screened Interaction
# | el_el_corr                       # [R] Electron-Electron Correlation
# | ElecTemp= 0.025861         eV    # Electronic Temperature
# | BoseTemp= 0.025861         eV    # Bosonic Temperature
# | NLogCPUs= 10                     # [PARALLEL] Live-timing CPU`s (0 for all)
# | FFTGvecs= 17655            RL    # [FFT] Plane-waves
# | X_and_IO_CPU= "1 1 1 60 2"       # [PARALLEL] CPUs for each role
# | X_and_IO_ROLEs= "q g k c v"      # [PARALLEL] CPUs roles (q,g,k,c,v)
# | X_and_IO_nCPU_LinAlg_INV=  36    # [PARALLEL] CPUs for Linear Algebra (if -1 it is automatically set)
# | X_Threads=  56                   # [OPENMP/X] Number of threads for response functions
# | DIP_CPU= "1 60 2"                # [PARALLEL] CPUs for each role
# | DIP_ROLEs= "k c v"               # [PARALLEL] CPUs roles (k,c,v)
# | DIP_Threads=  56                 # [OPENMP/X] Number of threads for dipoles
# | SE_CPU= "6.10.2"                 # [PARALLEL] CPUs for each role
# | SE_ROLEs= "b.qp.q"               # [PARALLEL] CPUs roles (q,qp,b)
# | SE_Threads=  56                  # [OPENMP/GW] Number of threads for self-energy
# | RandQpts= 3000000                # [RIM] Number of random q-points in the BZ
# | RandGvec= 1                RL    # [RIM] Coulomb interaction RS components
# | CUTGeo= "box z"                  # [CUT] Coulomb Cutoff geometry: box/cylinder/sphere/ws/slab X/Y/Z/XY..
# | % CUTBox
# |   0.00000 |  0.00000 | 68.00000 |        # [CUT] [au] Box sides
# | %
# | CUTRadius= 0.000000              # [CUT] [au] Sphere/Cylinder radius
# | CUTCylLen= 0.000000              # [CUT] [au] Cylinder length
# | CUTwsGvec= 0.000000              # [CUT] WS cutoff: number of G to be modified
# | EXXRLvcs= 110539           RL    # [XX] Exchange    RL components
# | VXCRLvcs= 110539           RL    # [XC] XCpotential RL components
# | Chimod= "HARTREE"                # [X] IP/Hartree/ALDA/LRC/PF/BSfxc
# | % BndsRnXp
# |     1 | 2000 |                       # [Xp] Polarization function bands
# | %
# | NGsBlkXp= 20               Ry    # [Xp] Response block size
# | % LongDrXp
# |  0.577350E-5 | 0.577350E-5 | 0.577350E-5 # [Xp] [cc] Electric Field
# | %
# | PPAPntXp= 1.000000         Ha    # [Xp] PPA imaginary energy
# | % GbndRnge
# |     1 | 2000 |                       # [GW] G[W] bands range
# | %
# | GTermKind= "none"                # [GW] GW terminator ("none","BG" Bruneval-Gonze,"BRS" Berger-Reining-Sottile)
# | DysSolver= "n"                   # [GW] Dyson Equation solver ("n","s","g","q")
# | %QPkrange                        # [GW] QP generalized Kpoint/Band indices
# | 61|61|35|36|
# | 61|61|40|41|
# | 1|1|38|39|
# | 1|1|44|45|
# | %
# | %QPerange                        # [GW] QP generalized Kpoint/Energy indices
# | 1|61| 0.000000|-1.000000|
# | %
