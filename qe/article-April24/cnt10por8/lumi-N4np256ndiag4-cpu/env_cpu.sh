module --force purge
module load CrayEnv
module load git/2.33.1
module spider cray-mpich
module load PrgEnv-cray
module load cce/14.0.2
module load cray-fftw
module load LUMI/22.08

export MPICH_GPU_SUPPORT_ENABLED=0
export ESPRESSO_PSEUDO=../../pseudo/
export QEROOT=`pwd`
