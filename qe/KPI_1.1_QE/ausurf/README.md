## Ausurf test for single node or few GPUs

* 800 bands and ~ 10^5 plane waves
* 2 k-points parallelized over 2 pool-groups 
* tested on one node on leonardo_boost@CINECA with 2 or 4 GPUs
* KPI 1.1:
    * test versions:
        * 7.2 march 2023 (baseline) t.t.s 66 2 GPU ;49 4 GPU secs
        * 7.3 december 2023         t.t.s 66 2 GPU ;48 4 GPU secs
        * 7.4 november 2024         t.t.s 66 2 GPU ;48 4 GPU secs 

