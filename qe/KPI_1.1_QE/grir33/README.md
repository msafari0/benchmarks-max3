## Graphene on Iridium slab 

* medium test case benchmarked on 4 nodes of leonardo\_boost@CINECA
* uses PAW datasets
* 1793 bands; ~3X10^5 plane waves
* KPI1.1:
    7.2 (march 2023) t.t.s 1042 secs 
    7.3 (december 2023) t.t.s. 874 secs
    7.4 (november 2024) t.t.s 659 secs
    * improvement w.r.t. baseline ~ 37%
    * relevant changes:
        * GPU acceleration of mix_rho
        * GPU acceleration of  PAW_pot
        * improvement of data movement and data locality
