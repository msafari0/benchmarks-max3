#! /bin/bash

#SBATCH --job-name=cntpor_8_nodes
#SBATCH --nodes=8        # number of nodes
#SBATCH --ntasks-per-node=4     # number of tasks per node
#SBATCH --cpus-per-task=8       # number of HW threads per task (equal to OMP_NUM_THREADS*4)
#SBATCH --gpus-per-node=4       # this refers to the number of requested gpus per node, and can vary between 1 and 4
#SBATCH --mem=480000MB
#SBATCH --time 2:59:00          # format: HH:MM:SS
#SBATCH -A Sis24_baroni
#SBATCH -p boost_usr_prod
##SBATCH --qos=boost_qos_dbg
##SBATCH  --qos=normal
#SBATCH --dependency=afterany:4503341
#

module load nvhpc/23.11 
module load openmpi/4.1.6--nvhpc--23.11
module load intel-oneapi-mkl/2023.2.0 
module load hdf5/1.14.3--openmpi--4.1.6--nvhpc--23.11  
export LD_LIBRARY_PATH=/leonardo/prod/spack/5.2/install/0.21/linux-rhel8-icelake/gcc-8.5.0/nvhpc-23.11-wnrvac5a7nx7cnbtaamktaaqljmgorva/Linux_x86_64/23.11/cuda/lib64/:$LD_LIBRARY_PATH

QEDIR72=/leonardo_work/Sis24_baroni/pdelugas/build_72_nvhpc/bin/
QEDIR73=/leonardo_work/Sis24_baroni/pdelugas/build_73_nvhpc/bin/
QEDIR74=/leonardo_work/Sis24_baroni/pdelugas/build_74_nvhpc/bin/
#BPROJDIR=/leonardo_scratch/large/userexternal/pdelugas/newproj_bin


export OMP_NUM_THREADS=8
#mpirun --map-by ppr:${SLURM_NTASKS_PER_NODE}:node:PE=${OMP_NUM_THREADS} --rank-by core $PW -nk 4 -i pw.in > pw_scf_h5_731.out 
mpirun --map-by ppr:${SLURM_NTASKS_PER_NODE}:node:PE=${OMP_NUM_THREADS} --rank-by core ${QEDIR72}pw.x   -i cnt10por8.in  > out_72
mpirun --map-by ppr:${SLURM_NTASKS_PER_NODE}:node:PE=${OMP_NUM_THREADS} --rank-by core ${QEDIR73}pw.x   -i cnt10por8.in  > out_73
mpirun --map-by ppr:${SLURM_NTASKS_PER_NODE}:node:PE=${OMP_NUM_THREADS} --rank-by core ${QEDIR74}pw.x   -i cnt10por8.in  > out_74

