# Fixed allocation tests for KPI 1.1

This directory collects fixed allocation tests for QE to monitor the impact of
code improvements related to intra process code improvements:
  * improvement of algorithms; 
  * host-device data movements; 
  * increase device data residence
  * GPU offloading of non accelerated  parts. 

Current tests are:
  * ausurf: small test with 2-kpoint run with 2 pools in 2 or 4 GPUs on leonardo
  * Water cluster with SCAN functional: Gamma point test run on 4 GPUs (64 water molecules) 
  * grir33 Graphene sheet on top of an Iridium slab 
  * cri3small Supercell of CrI3 (LSDA SCF + 1 shot non-collinear + spin-orbit total energy) 
  * cntpor: Carbon nanotube + attached porphyrines

Versions tested:
  * qe-7.2 (march 2023)  **baseline**
  * qe-7.3 (december 2023) 
  * qe-7.3.1 (february 2024) 
  * qe-7.4 (november 2024) 
