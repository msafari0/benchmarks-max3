#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TError.h" // kError
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TAxis.h"
#include "TStyle.h" // gStyle
#include "TLatex.h" // tex

#include <stdio.h>
#include <stdlib.h> // atoi


#define NODE 3456
#define GPU  4

//------------------------------------
int main(int argc, char * argv[]){
//--------------------------

  bool p = false;
  TString oPdf="report.pdf";

  unsigned int job=1234567;
  TString iTxt="enode.txt";
  TString iRoot="data.root";


  if(argc!=4){
    printf("Usage  : %s <rootfile> <job> <energy per node file>\n",argv[0]);
    printf("Example: %s %s %d %s\n",argv[0],iRoot.Data(),job,iTxt.Data());
     return -1;
  }else{
    iRoot  = argv[1];
    job    = atoi(argv[2]);
    iTxt   = argv[3];
  }

//  gROOT->Reset();
//  gErrorIgnoreLevel = kWarning;// kError;
//  gStyle->SetOptStat(0); // remove statbox

  TFile *f = new TFile(iRoot.Data());
  TTree* tree=(TTree*)f->Get("tree");

  if(tree==NULL){
    printf("data tree not found... Exit\n");
    return -1;
  }

  //tree->Show(1);  dump entry 1

  unsigned short absgpu;
  unsigned short node;
  unsigned char gpu;
  float time;
  float watt;
  unsigned short clk;
  unsigned short clk1;
  unsigned short celsius;
  unsigned short celsius1;
  unsigned short usage;
  unsigned short usage1;

  tree->SetBranchAddress("absgpu"  ,&absgpu);
  tree->SetBranchAddress("node"    ,&node);
  tree->SetBranchAddress("gpu"     ,&gpu);
  tree->SetBranchAddress("unixtime",&time);
  tree->SetBranchAddress("watt"    ,&watt);
  tree->SetBranchAddress("clk"     ,&clk);
  tree->SetBranchAddress("clk1"    ,&clk1);
  tree->SetBranchAddress("celsius" ,&celsius);
  tree->SetBranchAddress("celsius1",&celsius1);
  tree->SetBranchAddress("usage"   ,&usage);
  tree->SetBranchAddress("usage1"  ,&usage1);

  //Allocate and init memory
  TGraph * gr[NODE*GPU][4]; // 0 power, 1 clock, 2 temperature, 3 usage
  unsigned int point[NODE*GPU];
  int sel; // absolute GPU identifier = nodeID*4 + gpuID

  for(int z=0;z<NODE;z++){
    for(int j=0;j<GPU;j++){
      sel=z*GPU+j;
      for(int i=0;i<4;i++){
        gr[sel][i]= new TGraph();
      }
      point[sel]=0;
    }
  }

  // read data and fill histograms
  for (Int_t i=0; i<(Int_t)tree->GetEntries(); i++) {
    if(p)if(i%100000==0) printf("Event %d\n",i);
    tree->GetEntry(i);

    if(p){
      printf("%04d ",node);
      printf("%d "  ,gpu);
      printf("%16.6f ",time);
      printf("%6.2f ",watt);
      printf("%6d "  ,clk);
      printf("%5d "  ,clk1);
      printf("%5d "  ,celsius);
      printf("%5d "  ,celsius1);
      printf("%3d "  ,usage);
      printf("%3d "  ,usage1);
      printf("\n");
    }

    sel = (node-1)*GPU+gpu; // attention node-1 to align memory (starting from 0) with node IDs (starting from 1)
    if(sel<GPU*NODE){
      gr[sel][0]->SetPoint(point[sel], time, watt);
      gr[sel][1]->SetPoint(point[sel], time, clk);
      gr[sel][2]->SetPoint(point[sel], time, usage);
      gr[sel][3]->SetPoint(point[sel], time, celsius);
      point[sel]++;
    }
  } // end loop on Entries

  FILE * fin = fopen(iTxt.Data(),"r");
  float energy;
  char nodename[32];
  int ret;
  // continuous later (bad ) --> remove the title of the page or find a cleaner solution

  // Cosmetics
  float marker_style  = 20;
  float marker_size   = 0.3; // marker size is expressed arbitrary units, size=1 means a square of 8 pixels
  float line_width    = 1.5;
  float size_label    = 0.05;
  float size_title    = 0.05;
  float offset        = 1.25;
  float margin_left   = 0.12;
  float margin_bottom = 0.14;

  // Plot
  TCanvas *  canv = new TCanvas("canv", "canv", 1000, 500);
  canv->Divide(2,2);

  canv->Print(Form("%s[",oPdf.Data()));

  int k; // counter to check if the node is present and have to be plotted

  double ymax[4]={500,1900,110,85}; // Watt, MHz, Percentage, Celsius
  double ymin[4]={  0,   0,-10,35}; // Watt, MHz, Percentage, Celsius


  for(int z=0;z<NODE;z++){  // loop on nodes
    if(p)if(z%1000==0) printf("Node %d\n",z);
    k=0; // goes 1 only for nodes that have data

    TString title[4];
    title[0] = "; Time[s]; Power consumption [W]";
    title[1] = "; Time[s]; Clock [MHz]";
    title[2] = "; Time[s]; Usage [%]";
    title[3] = "; Time[s]; Temperature [Celsius]";

   // int color[4]={kGreen,kRed,kBlack,kBlue}; // multi-gpu node
    int color[4]={kBlue,kGreen,kRed,kBlack}; //single gpu

    TMultiGraph * mg[4]; // one multigraph per variable (each multigraph contains the 4 GPUs of a node)

    for(int i=0;i<4;i++){ // loop on variables pwr, clk,usage, temperature

      canv->cd(i+1)->SetGrid();
      canv->cd(i+1)->SetLeftMargin(margin_left);
      canv->cd(i+1)->SetBottomMargin(margin_bottom);

      mg[i]= new TMultiGraph();
      for(int j=0;j<GPU;j++){
        sel=z*GPU+j; // determine the index in the node aray
        if((gr[sel][i]->GetN())!=0){ // if the Graph exists, only the first graph is checked

          gr[sel][i]->SetMarkerStyle(marker_style);
          gr[sel][i]->SetMarkerSize(marker_size);
          gr[sel][i]->SetMarkerColor(color[j]);
          gr[sel][i]->SetLineWidth(line_width);
          gr[sel][i]->SetLineColor(color[j]);
          mg[i]->Add(gr[sel][i],"lp");
          k++;
        }
      }// end loop on GPU of a node

      if(k){
        mg[i]->SetTitle(title[i].Data());

        mg[i]->GetYaxis()->SetLabelSize(size_label);
        mg[i]->GetXaxis()->SetLabelSize(size_label);
        mg[i]->GetXaxis()->SetTitleSize(size_title);
        mg[i]->GetYaxis()->SetTitleSize(size_title);
        mg[i]->GetXaxis()->SetTitleOffset(offset);
        mg[i]->GetYaxis()->SetTitleOffset(offset);
        mg[i]->GetXaxis()->CenterTitle();
        mg[i]->GetYaxis()->CenterTitle();
        //mg[i]->GetXaxis()->SetLimits(0,600); // fixed X-axis range
        mg[i]->SetMaximum(ymax[i]);
        mg[i]->SetMinimum(ymin[i]);
        mg[i]->Draw("AP");


      } // if node has data (k!=0)
    } // end loop on variables


   if(k){
     ret =fscanf(fin,"%s %f",nodename,&energy);
     if(ret==0)printf("Error in reading iTxt.Data()\n");

     energy=12345;
     // Canvas Title
     canv->cd();
     TPad *pad5 = new TPad("all","all",0,0,1,1);
     pad5->SetFillStyle(4000);  // transparent
     pad5->Draw();
     pad5->cd();
     TLatex *   tex = new TLatex(0.32,0.96,Form("job %7d %s %3.1lf kJ",job,nodename,energy*1E-3));
     tex->SetNDC();
     tex->SetTextSize(0.05);
     tex->Draw();

     // print the page
     canv->Print(Form("%s",oPdf.Data()));
     delete tex;
     delete pad5;

   }

    // clean memory
    for(int i=0;i<4;i++){
      delete mg[i];
    }
  } // end loop on nodes
  canv->Print(Form("%s]",oPdf.Data()));
  if(p)printf("File %s ready\n",oPdf.Data());

  fclose(fin);
  return 0;
}
