#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include <stdio.h>

//------------------------------------
int main(int argc, char * argv[]){
//--------------------------

  for(int i=0; i<argc; i++){
    printf("arg[%d] = %s\n",i,argv[i]);
  }

  TString ifile = "data.txt";
  TString ofile = "data.root";
  if(argc!=3){
    printf("Usage  : %s <inputfile> <output>\n",argv[0]);
    printf("Example: %s %s %s\n",argv[0],ifile.Data(),ofile.Data());
    return -1;
  }else{
    ifile = argv[1];
    ofile = argv[2];
  }

 // TODO: fix format, with or without node ID?
//  const char *descriptor = "id/s:gpu/b:unixtime/F:watt/F:clk/s:clk1/s:celsius/s:celsius1/s:usage/s:usage1/s";
//  const char *descriptor = "gpu/b:unixtime/F:watt/F:clk/s:clk1/s:celsius/s:celsius1/s:usage/s:usage1/s";
  //const char *descriptor = "absgpu/s:node/s:gpu/b:unixtime/F:watt/F:clk/s:clk1/s:celsius/s:celsius1/s:usage/s:usage1/s:dgpu/b:dt/F:dpwr/F:dclk/S:dclk1/S:dcelsius/S:dcelsius1/S:dusage/b:dusage1/b";


// Descriptor symbols useful
// s = 16-bit unsigned integer 16 bit unsigned short
// S = 16-bit unsigned integer 16 bit signed short
// b =  8-bit unsigned integer  8 bit unsigned integer
// F = 32-bit floating point float
// C = character string terminated by the 0 character
// more info here line 91, https://root.cern.ch/doc/master/TTree_8cxx_source.html#l07564

  // value only
//  const char *descriptor = "absgpu/s:node/s:gpu/b:unixtime/F:watt/F:clk/s:clk1/s:celsius/s:celsius1/s:usage/s:usage1/s";


  // value and difference
  std::string descr;
  descr+="absgpu/s";
  descr+=":node/s";
  descr+=":time/F";
  descr+=":gpu/b";
  descr+=":watt/F";
  descr+=":clk/s:clk1/s";
  descr+=":celsius/b:celsius1/b";
  descr+=":usage/b:usage1/b";
  descr+=":pstate/b";

  descr+=":dt/F";
  descr+=":dgpu/B";
  descr+=":dwatt/F";
  descr+=":dclk/S:dclk1/S";
  descr+=":dcelsius/B:dcelsius1/B";
  descr+=":dusage/B:dusage1/B";
  descr+=":dpstate/B";

  const char *descriptor = descr.c_str();

  const char *oRoot = ofile.Data();

  TFile * f = new TFile(oRoot,"RECREATE");
  if(f->IsZombie()){
    printf("Error opening file %s\n",oRoot);
  }

  TTree * T = new TTree("tree","data from ascii file");
  Long64_t nlines = T->ReadFile(argv[1],descriptor);
  T->Write();
  printf("File %s ready with %lld entries\n",oRoot,nlines);

  f->Close();
  return 0;
}
