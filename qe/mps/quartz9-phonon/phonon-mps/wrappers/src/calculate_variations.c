#include <stdio.h>
#include <stdlib.h> // string to double strtod


#define NCOL 10
//---------------------------------
int main(int argc,char * argv[]){
//---------------------------------

  char itxt[80];
  sprintf(itxt,"%s","input_filename.txt");

  char otxt[80];
  sprintf(otxt,"%s","output_filename.txt");

  if(argc!=3){
    printf("Usage  : %s <ifilename> <ofilename>\n",argv[0]);
    printf("Example: %s %s %s\n",argv[0],itxt,otxt);
    return -1;
  }else{
    sprintf(itxt,"%s",argv[1]);
    sprintf(otxt,"%s",argv[2]);
  }

  FILE * fin =fopen(itxt,"r");
  FILE * fout =fopen(otxt,"w");

  float val[NCOL];
  float prev[NCOL];
  float diff[NCOL];
  int  p=0; // debug

  // reset
  for(int i=0;i<NCOL;i++){diff[i]=0;}


  int line=0;
  while(fscanf(fin,"%f\n",val)!=EOF){

    // read a line
    for(int i=1;i<NCOL;i++){fscanf(fin,"%f",val+i);}

    if(p){
    // print current  line
      printf("%d",line); for(int i=0;i<NCOL;i++){printf("%12.3f ",val[i]);} printf("\n");
      // print previous line  (mmory buffer)
      printf("%d",line); for(int i=0;i<NCOL;i++){printf("%12.3f ",prev[i]);} printf("\n");
    }

    // compute the difference and update  the memory buffer
    if(line>0){
      for(int i=0;i<NCOL;i++){
        diff[i]=val[i]-prev[i];
        prev[i]=val[i];
       }
    }

    if(p){
      // print difference
      printf("%d",line); for(int i=0;i<NCOL;i++){printf("%12.4f ",diff[i]);} printf("\n");
    }

    // stream on outputfile
    for(int i=0;i<NCOL;i++){

      if(i==0)fprintf(fout,"%6.3f ",diff[i]);
      else if(i==2)fprintf(fout,"%7.2f ",diff[i]);
      else fprintf(fout,"%6.0f ",diff[i]);
    }
    fprintf(fout,"\n");

    // update line counter
    line++;
  } // end of while

fclose(fin);
fclose(fout);
return 0;
}
