#!/bin/bash
SRC="vis"

IDIR="./data/3955472/"
if [[ $# -ne 1 ]];then
  echo "Description: GPU energy consumption calculator"
  echo "Note       : For jobs on Leonardo Booster (CINECA/EuroHPC JU)"
  echo "Usage      : $0 <path to data>"
  echo "Example    : $0 $IDIR"
  exit 666
else
  IDIR=$1
fi
##############################################################
# Get jobid
JOB=$(basename "$IDIR")

echo "************"
echo "* Compile  *"
echo "************"
cd $SRC
make
cd -

echo "*******************"
echo "* Ingest all.txt  *"
echo "*******************"
EXE="ingest.x"
ITXT="all.txt"            # 11 cols
ITXT="all_with_diff.txt"  # 20 cols, include difference between adjacent samples

cp $SRC/$EXE .
./$EXE $ITXT # produce data.root
rm $EXE

echo "*****************"
echo "* Analise data *"
echo "*****************"
EXE="doHisto.x"
cp $SRC/$EXE .
./$EXE data.root #create histo.root
rm $EXE

EXE="plotHisto.x"
cp $SRC/$EXE .
./$EXE histo.root
rm $EXE



echo "*****************"
echo "* Create Report *"
echo "*****************"
# grab the executable, use it and clean up
EXE="report.x"
ITXT="enode.txt"
cp $SRC/$EXE .
./$EXE data.root $JOB $ITXT
rm $EXE
exit
#eof
