#!/bin/bash

node=$(hostname -s)
ITXT=$node.pdu.pid
nvpid=$(< $ITXT)
kill $nvpid
#kill -9 $nvpid
echo "killed process $nvpid on $node"

exit
