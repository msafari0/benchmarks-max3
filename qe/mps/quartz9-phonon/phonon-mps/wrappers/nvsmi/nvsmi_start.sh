#!/bin/bash
#This script does 2 things:
# 1 - dump configuration settings (.log, .cfg)
# 2 - start the monitoring process (.txt)

node=$(hostname | awk 'BEGIN { FS = "." } ; {print $1}')

############################
# GPU Configuration settings
# composed by two parts:
# 1 - generic and complete (human friendly)
# 2 - specific (computer friendly)
############################
nvidia-smi -q    > $node.nvsmi.log

# Define queries
queries=""
queries+="index"
queries+=",timestamp"
queries+=",clocks.sm"      # current
queries+=",clocks.gr"      # current
queries+=",clocks.mem"     # current
queries+=",clocks.video"   # current
queries+=",clocks.max.sm"  # max
queries+=",clocks.max.gr"  # max
queries+=",clocks.max.mem" # max
queries+=",clocks.applications.gr"          # base (when running)
queries+=",clocks.applications.mem"         # base (when running)
queries+=",clocks.default_applications.gr"  # default
queries+=",clocks.default_applications.mem" # default

# Define format
format=""
format+="csv" # mandatory
#format+=",noheader"
format+=",nounits"

nvidia-smi --query-gpu=$queries --format=$format > $node.nvsmi.cfg


##################################################################
# GPU monitoring (non blocking, please note the & symbol at the end)
##################################################################

# Define queries
queries=""
queries+="index"
queries+=",timestamp"
queries+=",power.draw"
queries+=",clocks.sm"
queries+=",clocks.mem"
queries+=",temperature.gpu"
queries+=",temperature.memory"
queries+=",utilization.gpu"
queries+=",utilization.memory"
queries+=",pstate"

# Define format
format=""
format+="csv"
format+=",noheader"
format+=",nounits"

# Define period
period=5

# monitoring
#nvidia-smi --query-gpu=$queries --format=$format -lms $period >> $node.nvsmi.txt 2>&1 &
nvidia-smi --query-gpu=$queries --format=$format -lms $period > $node.nvsmi.txt &

# the process in background will be killed using 'pkill' command,
# however an alternative version based on process id (PID) follows
#nvpid=$!
#echo "nvsmi monitoring launched on $node with PID $nvpid"
#echo $nvpid > $node.nvsmi.pid

#eof
