#!/bin/bash

label=$(hostname -s)
SLEEP=1
# Polling rate test:
#./getpower.sh acnode04 10 | wc -l
# 10 seconds
# header line is included (e.g. 11 lines -> 10 measurements)
#  sleep 0.001 # 90,94,96,96,98 expected 10001
#  sleep 0.01  # 89,84,92,89,85 expected 1001
#  sleep 0.1   # 50,52,49,52,52 expected 101
#  sleep 1     # 11,11,11,11,11 expected 11


label=$(echo $label | tr '[:lower:]' '[:upper:]')
deviceid=$(curl -q -s --location --request GET 'http://opendcim.e4srv/api/v1/device?Label='$label --header 'Authorization: Basic Z3Vlc3Q6Z3Vlc3Q=' --header 'Cookie: PHPSESSID=mmnqsqdudfpfr80epu6gimr7i6'| jq -r .device[].DeviceID)
powerdevice=$(curl -q -s --location --request GET 'http://opendcim.e4srv/api/v1/powerport/'$deviceid --header 'Authorization: Basic Z3Vlc3Q6Z3Vlc3Q=' --header 'Cookie: PHPSESSID=mmnqsqdudfpfr80epu6gimr7i6' | jq -r .powerport[].ConnectedDeviceID | xargs)


# device filter
#stringarray=($powerdevice)
#echo ${stringarray[0]} # 83 not responding
#echo ${stringarray[1]} # 81
#powerdevice=${stringarray[1]}


#echo "label $label"
#echo "deviceid $deviceid"
#echo "powerdevice $powerdevice"

SECONDS=0
#echo "time,watts,va"


while true; do
  totalpower=0
  totalactive=0
  for power in $powerdevice; do
    powerport=$(curl -q -s --location --request GET 'http://opendcim.e4srv/api/v1/powerport/'$deviceid --header 'Authorization: Basic Z3Vlc3Q6Z3Vlc3Q=' --header 'Cookie: PHPSESSID=mmnqsqdudfpfr80epu6gimr7i6' | jq -r '.powerport[] | select(.ConnectedDeviceID=="'$power'")'.ConnectedPort)
    ip=$(curl -q -s --location --request GET 'http://opendcim.e4srv/api/v1/device/'$power --header 'Authorization: Basic Z3Vlc3Q6Z3Vlc3Q=' --header 'Cookie: PHPSESSID=mmnqsqdudfpfr80epu6gimr7i6' | jq -r .device.PrimaryIP)


    readingpower=$(snmpwalk -c public -v1 $ip .1.3.6.1.4.1.534.6.6.7.6.5.1.3.0.$powerport | awk '{print $NF}')
    (( totalpower=readingpower+totalpower ))
    readingactive=$(snmpwalk -c public -v1 $ip .1.3.6.1.4.1.534.6.6.7.6.5.1.2.0.$powerport | awk '{print $NF}')
    (( totalactive=readingactive+totalactive ))

 #   printf "power device %s " $power
 #   printf "port %s " $powerport
 #   printf "ip %s " $ip
 #   printf "reading power %s " $readingpower
 #   printf "reading active %s" $readingactive
 #   printf "\n"
  done
  DATE=$(date "+%Y/%m/%d")
  TIME=$(date "+%T.%3N")
  printf "%10s" $DATE
  printf "%13s" $TIME
  printf "%5s" $totalpower
  printf "%5s" $totalactive
  printf "\n"
 # echo "$now,$totalpower,$totalactive"
  sleep $SLEEP
done


exit





