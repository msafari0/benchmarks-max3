#!/bin/bash

IDIR="./data/3955472/"
if [[ $# -ne 1 ]];then
  echo "Description: GPU energy profiler"
  echo "Usage      : $0 <path to data>"
  echo "Example    : $0 $IDIR"
  exit 666
else
  IDIR=$1
fi

# check nodelist exists
printf -v NODELIST "%s/nodelist.txt" $IDIR
if [ -f "$ITXT" ]; then
echo "File $NODELIST exists"
fi

echo "***************"
echo " Convert Time  "
echo "***************"
while read -r node ; do # Loop on nodes

  for what in nvsmi pdu; do
    ITXT="$node.$what.txt.good"
    if [ -f "$ITXT" ]; then
      echo "./process_timestamp.sh $ITXT"
      ./process_timestamp.sh $ITXT
    fi
  done
done < $NODELIST



echo "***************************"
echo " T0 selection (time sorting the first sample of each raw file"
echo "***************************"
rm -rf tmp.txt
while read -r node ; do # Loop on nodes

  for what in nvsmi pdu; do
    ITXT="$node.$what.txt.good.unixtime"
    if [ -f "$ITXT" ]; then
      VAL=$(head -n 1 $ITXT)
      echo $VAL >> tmp.txt
    fi

  done
done < $NODELIST
sort -n tmp.txt > tmp2.txt

T0=$(head -n 1 tmp2.txt)
rm -rf tmp.txt
rm -rf tmp2.txt

echo $T0
echo $T0 > offset.t0.txt
date -d "@$T0" >> offset.t0.txt

echo "***************************"
echo " T0 subtraction"
echo "***************************"

while read -r node ; do # Loop on nodes
  for what in nvsmi pdu; do
    ITXT="$node.$what.txt.good.unixtime"
    if [ -f "$ITXT" ]; then
      OTXT="$node.$what.txt.good.unixtime.align"
      ./src/subtract_offset.x $ITXT $T0 $OTXT
    fi
  done
done < $NODELIST



echo "***************************"
echo " Paste "
echo "***************************"

while read -r node ; do # Loop on nodes


  # GPU
  what="nvsmi"
  ITXT1="$node.$what.txt.good.unixtime.align"
  ITXT2="$node.$what.txt.good.other"
  if [ -f "$ITXT1" ]; then
    OTXT1="$node.$what.txt.good.unixtime.almost1"
    paste $ITXT1 $ITXT2 > $OTXT1
    OTXT2="$node.$what.txt.good.unixtime.almost2"
    sed 's/,/ /g' $OTXT1 | sed 's/P/ /g' > $OTXT2

    # formatted output
    OTXT3="$node.$what.txt.good.unixtime.ready"
    LC_ALL="C" awk '{printf("%8.3f %2d %8.2f %6d %6d %6d %6d %6d %6d %6d\n",$1,$2,$3,$4,$5,$6,$7,$8,$9,$10)}' $OTXT2 > $OTXT3

    # clean up temporary files
    rm $OTXT1
    rm $OTXT2
    rm $ITXT1
    rm $ITXT2
  fi

  # PDU
  what="pdu"
  ITXT1="$node.$what.txt.good.unixtime.align"
  ITXT2="$node.$what.txt.good.other"
  if [ -f "$ITXT1" ]; then

    OTXT1="$node.$what.txt.good.other.prw1"
    OTXT2="$node.$what.txt.good.other.prw2"
    # split the two power measurements from PDU
    awk '{print $1}' $ITXT2 > $OTXT1
    awk '{print $2}' $ITXT2 > $OTXT2

    OTXT3="$node.$what.txt.good.other.pdu1"
    OTXT4="$node.$what.txt.good.other.pdu2"

    paste $ITXT1 $OTXT1 > $OTXT3
    paste $ITXT1 $OTXT2 > $OTXT4

    OTXT5="$node.$what.txt.good.other.pdu1.ready"
    OTXT6="$node.$what.txt.good.other.pdu2.ready"

    pdu=0
    LC_ALL="C" awk -v x=$pdu '{printf("%8.3f %2d %6.3f\n",$1,x+1,$2)}' $OTXT3 > $OTXT5
    pdu=1
    LC_ALL="C" awk -v x=$pdu '{printf("%8.3f %2d %6.3f\n",$1,x+1,$2)}' $OTXT4 > $OTXT6

    OTXT7="$node.$what.txt.good.other.pdu.all"
    cat $OTXT5 $OTXT6 > $OTXT7

    # formatted output
    OTXT8="$node.$what.txt.good.unixtime.ready"
    DUMMY="0"
    LC_ALL="C" awk -v x=$DUMMY '{printf("%8.3f %2d %8.2f %6s %6s %6s %6s %6s %6s %6s\n",$1,$2,$3,x,x,x,x,x,x,x)}' $OTXT7 > $OTXT8

    rm $OTXT1
    rm $OTXT2
    rm $OTXT3
    rm $OTXT4
    rm $OTXT5
    rm $OTXT6
    rm $OTXT7

    rm $ITXT1
    rm $ITXT2

  fi

done < $NODELIST

exit






