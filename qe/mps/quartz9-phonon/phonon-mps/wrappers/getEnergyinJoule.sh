#!/bin/bash

IDIR="./data/3955472/"
if [[ $# -ne 1 ]];then
  echo "Description: GPU energy profiler"
  echo "Usage      : $0 <path to data>"
  echo "Example    : $0 $IDIR"
  exit 666
else
  IDIR=$1
fi


SRC="./src"

echo "***************************"
echo " Datetime2Timestamp and T0 "
echo "***************************"
SCRIPT=convert_time.sh
EXE=subtract_offset.x
cp $SRC/$SCRIPT .
cp $SRC/$EXE .
printf -v NODELIST "%s/nodelist.txt" $IDIR
while read -r node ; do # Loop on nodes

  ITXT="$node.nvsmi.txt.good"
  OTXT1="$node.nvsmi.txt.good.timestamp"
  OTXT2="$node.nvsmi.txt.good.t0"

  ./convert_time.sh $ITXT $OTXT1 $OTXT2
done < $NODELIST
rm $SCRIPT
rm $EXE


echo "***********************"
echo " Energy per GPU (Joule)"
echo "***********************"
EXE1=calculate_variations.x
EXE2=calculate_integral.x
cp $SRC/$EXE1 .
cp $SRC/$EXE2 .

NGPU=0

printf -v NODELIST "%s/nodelist.txt" $IDIR
while read -r node ; do # Loop on nodes

  ITXT="$node.nvsmi.txt.good.timestamp"
  for gpu in 0 1 2 3; do
    let NGPU++

    # split data by GPU
    printf -v OTXT1 "%s.nvsmi.txt.good.timestamp.%d" $node $gpu
    awk -v x=$gpu '$1==x {print $0}' $ITXT > $OTXT1

    # calculate difference between samples (dt)
    printf -v OTXT2 "%s.nvsmi.txt.good.timestamp.%d.diff" $node $gpu
    ./$EXE1 $OTXT1 $OTXT2

    # calculate energy by integrating the power absorption (pwr) over time (dt)
    printf -v OTXT3 "%s.nvsmi.txt.good.timestamp.%d.pwr" $node $gpu
    printf -v OTXT4 "%s.nvsmi.txt.good.timestamp.%d.dt" $node $gpu
    printf -v OTXT5 "%s.nvsmi.txt.good.timestamp.%d.energy" $node $gpu
    awk '{print $3}' $OTXT1 > $OTXT3 # power
    awk '{print $2}' $OTXT2 > $OTXT4 # dt
    ./$EXE2 $OTXT3 $OTXT4 $OTXT5 # energy
    cat $OTXT5 # dump energy value

  done
done < $NODELIST
rm $EXE1
rm $EXE2

echo "********************"
echo " Energy per NODE (Joule)"
echo "********************"
printf -v NODELIST "%s/nodelist.txt" $IDIR
TXT=lrdn_nodes.txt
TMP=tmp.txt
rm -f $TXT
while read -r node ; do # Loop on nodes
  # collect individual GPU energy consumption (scalar)
  rm -f $TMP
  for gpu in 0 1 2 3; do
    printf -v ITXT1 "%s.nvsmi.txt.good.timestamp.%d.energy" $node $gpu
    cat $ITXT1 >> $TMP
  done

  # sum up all the energy values of a node
# TODO: sum as floating point numbers (currenct caluclation is integers!)
  ENERGY=$(awk '{sum+=$1}; END {printf "%f\n", sum}' $TMP)
  echo "$node $ENERGY Joules"
  echo $node $ENERGY >> $TXT
  cat $TMP

done < $NODELIST
rm -f $TMP



echo "*******************"
echo " Energy per JOB (Joule)"
echo "*******************"
printf -v NODELIST "%s/nodelist.txt" $IDIR
TXT=lrdn_gpus.txt
rm -f $TXT
while read -r node ; do # Loop on nodes
  for gpu in 0 1 2 3; do
    printf -v ITXT1 "%s.nvsmi.txt.good.timestamp.%d.energy" $node $gpu
    cat $ITXT1 >> $TXT
  done
done < $NODELIST


# TODO: sum as floating point numbers (currenct caluclation is integers!)
ENERGY=$(awk '{sum+=$1}; END {printf "%f\n", sum}' $TXT)
echo "JOB $JOB have $NGPU GPU(s) absorbing a total $ENERGY Joules"
echo $ENERGY > ejob.txt





echo "***************************************"
echo " Merge processed data in a single file"
echo "****************************************"
OTXT="all.txt"
rm -f $OTXT
NGPU=0
while read -r node ; do # Loop on nodes
#  NODEID=${node:4:4} # extract node ID = 0123 # Leonardo lrdn1234
  NODEID=${node:6:2} # extract node ID = 04 # E4 acnode04


  for gpu in 0 1 2 3; do
    let NGPU++
    printf -v ITXT "%s.nvsmi.txt.good.timestamp.%d" $node $gpu
    awk -v y=$NODEID -v x=$NGPU '{printf "%s %s %s\n",x,y,$0}' $ITXT >> $OTXT
    # the format of OTXT is "absolute GPU, node ID, local GPU, timestamps,... and all other nvsmi variables"
  done
done < $NODELIST


echo "***************************************"
echo " Merge processed data and difference between adjacent samples (under development)"
echo "****************************************"
OTXT="all_with_diff.txt"
TMP0="temp0.txt"
TMP1="temp1.txt"
rm -f $OTXT
rm -f $TMP0
rm -f $TMP1
NGPU=0

while read -r node ; do # Loop on nodes
#  NODEID=${node:4:4} # extract node ID = 0123 # Leonardo lrdn1234
  NODEID=${node:6:2} # extract node ID = 04 # E4 acnode04
  for gpu in 0 1 2 3; do
    let NGPU++
    printf -v ITXT0 "%s.nvsmi.txt.good.timestamp.%d" $node $gpu
    printf -v ITXT1 "%s.nvsmi.txt.good.timestamp.%d.diff" $node $gpu
    awk -v y=$NODEID -v x=$NGPU '{printf "%s %s %s\n",x,y,$0}' $ITXT0 > $TMP0
    awk '{printf "%s\n",$0}' $ITXT1 >  $TMP1
    paste $TMP0 $TMP1 >> $OTXT
  done
done < $NODELIST
rm -f $TMP0
rm -f $TMP1


# clean up temporary files
TXT=lrdn_nodes.txt
mv lrdn_nodes.txt enode.txt
mv lrdn_gpus.txt egpu.txt
#rm -f lrdn*

echo "File $OTXT ready"
exit

