#!/bin/bash
dir=${PWD}

# stop nvidia-smi on all nodes
for node in `scontrol show hostname`; do
  exe="wrappers/nvsmi/nvsmi_stop.sh"
  ssh $node "cd $dir && $exe"
done

# Export run LOG file
LOG=${PWD}/file.log
echo $NAME       >  $LOG
echo $APP        >> $LOG
echo $SLURM_JOB_NAME >> $LOG
echo $START      >> $LOG
echo $START_UNIX >> $LOG
echo $STOP       >> $LOG
echo $STOP_UNIX  >> $LOG
echo $SLURM_JOB_NUM_NODES >> $LOG # Number of nodes allocated
echo $SLURM_JOB_NODELIST  >> $LOG # Nodes assigned to the job
echo $SLURM_NODEID        >> $LOG # node index withtin node list
echo $SLURM_LOCALID       >> $LOG  # core index on local node
echo $SLURM_JOBID       >> $LOG  # JOBID

echo "Default"            >> $LOG # TODO: read with nvidia smi + bash code

