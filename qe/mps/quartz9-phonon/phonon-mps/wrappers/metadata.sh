#!/bin/bash

IDIR="./data/3955472/"
if [[ $# -ne 1 ]];then
  echo "Description: GPU energy profiler"
  echo "Usage      : $0 <path to data>"
  echo "Example    : $0 $IDIR"
  exit 666
else
  IDIR=$1                           # data location
  JOB=$(basename "$IDIR")           # job id
  printf -v LOG "%s/file.log" $IDIR # file with job information (metadata)
  if [ ! -f $LOG ]; then
    echo "File $LOG not found. Exit"
    exit
  fi
fi
####################
VER="0.0" # one column: single values
#VER="1.0" # two columns: key values
#######################
case $VER in
  0.0) # single column
    CODE=$(sed '2q;d' $LOG)
    START=$(sed '5q;d' $LOG)
    STOP=$(sed '7q;d' $LOG)
    NODE=$(sed '8q;d' $LOG) # read the 8th line
    TTS=$(expr $STOP - $START )
    ;;

  1.0) #two columns key-value
    CODE=$(sed '2q;d' $LOG  | awk '{print $2}')
    START=$(sed '6q;d' $LOG | awk '{print $2}')
    STOP=$(sed '8q;d' $LOG  | awk '{print $2}')
    NODE=$(sed '9q;d' $LOG  | awk '{print $2}') # read second field of the 9th line
    TTS=$(($STOP-$START ))
    ;;
  *)
    echo "Unknown version $VER"
    exit
    ;;
esac

echo "**************"
echo "  Metadata "
echo "**************"
printf "%10s %10s %10s %10s\n" "app" "jobid" "nodes" "Duration"
printf "%10s %10s %10s %10s\n" "" "" "[#]" "[s]"
printf "%10s %10s %10s %10s\n" $CODE $JOB $NODE $TTS





exit
#eof
