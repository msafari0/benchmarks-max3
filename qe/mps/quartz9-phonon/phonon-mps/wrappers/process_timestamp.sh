#!/bin/bash

ITXT="../log/2392589/lrdn0579.nvidiasmi.txt"
if [[ $# -ne 1 ]];then
  echo "Usage  : $0 <input nvidia-smi file>"
  echo "Example: $0 $ITXT"
  exit 666
else
  ITXT=$1
fi

LC_ALL="C"


# device specific parameters
if [[ $ITXT == *"pdu"* ]]; then
  FIELD="1,2" # PDU
else
  FIELD="2,3" # NVSMI
fi


#echo "Split Date and Time from other variables"
cut -d" " -f $FIELD  $ITXT              > $ITXT.datetime # 0
cut -d" " -f $FIELD  --complement $ITXT > $ITXT.other   # 1


echo "File $ITXT.datetime ready"
echo "File $ITXT.other ready"

#echo "Convert Date and Time to unix timestamp"
./src/convert_date_time_to_unixtime.x $ITXT.datetime $ITXT.unixtime
echo "File $ITXT.unixtime ready"


#eof
