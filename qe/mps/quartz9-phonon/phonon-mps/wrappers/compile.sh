#!/bin/bash
echo "*****************************"
echo "  Compiling C/C++ Executables "
echo "****************************"
cd src && make && cd -

echo "****************************"
echo "* Compile  C++/ROOT Executables "
echo "****************************"

cd vis && make && cd -

#eof
