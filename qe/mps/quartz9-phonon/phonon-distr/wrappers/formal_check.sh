#!/bin/bash

IDIR="./data/3955472/"
if [[ $# -ne 1 ]];then
  echo "Description: GPU energy profiler"
  echo "Usage      : $0 <path to data>"
  echo "Example    : $0 $IDIR"
  exit 666
else
  IDIR=$1
fi


SRC="./src"


echo "**************"
echo " Formal check"
echo "**************"
printf "%10s %10s %10s %10s %10s\n" "node" "what" "lines" "good" "bad"

printf -v NODELIST "%s/nodelist.txt" $IDIR
while read -r node ; do # Loop on nodes

  TIMESERIES="$IDIR/$node.nvsmi.txt"
  NLINES=$(wc -l  $TIMESERIES | awk '{print $1}')

  # FORMAL CHECK ON THE NUMBER OF COLUMNS
  POSTDAQ_good="$node.raw.nvsmi.kept"
  POSTDAQ_bad="$node.raw.nvsmi.dropped"
  awk 'NF==11 {print}{}' $TIMESERIES > $POSTDAQ_good
  awk 'NF!=11 {print}{}' $TIMESERIES > $POSTDAQ_bad
  GOOD=$(wc -l $POSTDAQ_good | awk '{print $1}')
  BAD=$(wc -l $POSTDAQ_bad   | awk '{print $1}')

  printf "%10s %10s %10s %10s %10s\n" $node "nvsmi" $NLINES $GOOD $BAD

  OTXT="$node.nvsmi.txt.good"
  cp $POSTDAQ_good $OTXT
  rm -f  $POSTDAQ_good
  rm -f  $POSTDAQ_bad


  # optional PDU part (E4 setup)
  TIMESERIES="$IDIR$node.pdu.txt"
  if [ -f $TIMESERIES ]; then
    NLINES=$(wc -l  $TIMESERIES | awk '{print $1}')

    # FORMAL CHECK ON THE NUMBER OF COLUMNS
    POSTDAQ_good="$node.raw.nvsmi.kept"
    POSTDAQ_bad="$node.raw.nvsmi.dropped"
    awk 'NF==4 {print}{}' $TIMESERIES > $POSTDAQ_good
    awk 'NF!=4 {print}{}' $TIMESERIES > $POSTDAQ_bad
    GOOD=$(wc -l $POSTDAQ_good | awk '{print $1}')
    BAD=$(wc -l $POSTDAQ_bad   | awk '{print $1}')

    printf "%10s %10s %10s %10s %10s\n" $node "pdu" $NLINES $GOOD $BAD

    OTXT="$node.pdu.txt.good"
    cp $POSTDAQ_good $OTXT
    rm -f  $POSTDAQ_good
    rm -f  $POSTDAQ_bad
  fi
done < $NODELIST


exit
