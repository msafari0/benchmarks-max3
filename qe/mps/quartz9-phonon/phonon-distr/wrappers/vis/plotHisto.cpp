#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TError.h" // kError
#include "TH1F.h"
#include "TStyle.h"
#include "TPaveStats.h"

#include <stdio.h>
#include <stdlib.h> // atoi



//------------------------------------------------------
template<typename T>
void cosmetics(T histo, int color=2, int style=3344){
//------------------------------------------------------

  float ymin=1e-1;
  float ymax=1e+5;

  histo->SetLineColor(color);
  histo->SetFillColor(color);
  histo->SetFillStyle(style);
  histo->SetMinimum(ymin);
  histo->SetMaximum(ymax);

}

//------------------------------------------------------
template<typename T>
void drawSuperimposed(T h1, T h2){
//------------------------------------------------------

  h1->Draw();
  gPad->Update(); // force canv update. This will generate the stats object

  double yA,yB,newyA,newyB;
  TPaveStats * sb1 = (TPaveStats*)gPad->GetPrimitive("stats");
  yA = sb1->GetY1NDC();
  yB = sb1->GetY2NDC();
  newyA = 2 * yA - yB;
  newyB = yA;
  sb1->SetY1NDC(newyA);
  sb1->SetY2NDC(newyB);
  sb1->SetTextColor(kBlue);
  sb1->SetName("h1stats");

  h2->Draw("sames");
  gPad->Update();
  TPaveStats * sb2 = (TPaveStats*)gPad->GetPrimitive("stats");
  if(sb2)sb2-> SetTextColor(kRed);

}



//------------------------------------
int main(int argc, char * argv[]){
//--------------------------

  TString iRoot="histo.root";
  TString oPdf="histo.pdf";

  if(argc!=2){
    printf("Usage  : %s <rootfile>\n",argv[0]);
    printf("Example: %s %s\n",argv[0],iRoot.Data());
     return -1;
  }else{
    iRoot  = argv[1];
  }

  std::unique_ptr<TFile> myFile( TFile::Open(iRoot.Data()) );

  TH1F *hPower;
  myFile->GetObject("hPower",hPower);
  TH1S *hClockCore; myFile->GetObject("hClockCore",hClockCore);
  TH1S *hClockMemo; myFile->GetObject("hClockMemo",hClockMemo);
  TH1C *hTempeCore; myFile->GetObject("hTempeCore",hTempeCore);
  TH1C *hTempeMemo; myFile->GetObject("hTempeMemo",hTempeMemo);
  TH1C *hUsageCore; myFile->GetObject("hUsageCore",hUsageCore);
  TH1C *hUsageMemo; myFile->GetObject("hUsageMemo",hUsageMemo);

  if(!hClockCore) printf("Histo not found\n");
  if(!hTempeCore) printf("Histo not found\n");
  if(!hUsageCore) printf("Histo not found\n");
  if(!hClockMemo) printf("Histo not found\n");
  if(!hTempeMemo) printf("Histo not found\n");
  if(!hUsageMemo) printf("Histo not found\n");

   // GPU Board
  int color = kGreen+3;
  int style = 3344;
  cosmetics(hPower,color,style);

  // GPU core
  color = kBlue;
  style = 3354;
  cosmetics(hClockCore,color,style);
  cosmetics(hTempeCore,color,style);
  cosmetics(hUsageCore,color,style);

  // GPU Memory
  color = kRed;
  style = 3345;
  cosmetics(hClockMemo,color,style);
  cosmetics(hTempeMemo,color,style);
  cosmetics(hUsageMemo,color,style);

  // plotting
  TCanvas * canv = new TCanvas("canv","",1600,900);
  canv->cd()->SetGrid(1);
  canv->cd()->SetLogy(1);

  canv->Print(Form("%s[",oPdf.Data()));

  // set stats options (show histo title, nEntries, mean, rms)
//  int mode =2211;
//  gStyle->SetOptStat(mode);

  // page 1 - Power
  hPower->Draw();
  canv->Print(Form("%s",oPdf.Data()));

  // page 2 - Temperature
  drawSuperimposed(hTempeCore,hTempeMemo);
  canv->Print(Form("%s",oPdf.Data()));

  // page 3 - Clock
  drawSuperimposed(hClockCore,hClockMemo);
  canv->Print(Form("%s",oPdf.Data()));

  // page 4 - Usage
  drawSuperimposed(hUsageCore,hUsageMemo);
  canv->Print(Form("%s",oPdf.Data()));

  canv->Print(Form("%s]",oPdf.Data()));
  return 0;
}




