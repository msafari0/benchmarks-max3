#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TH1S.h"
#include "TH1C.h"
#include <stdio.h>

//------------------------------------
int main(int argc, char * argv[]){
//--------------------------

  bool p = true;
//  bool p = false;

  TString oRoot="histo.root";
  TString iRoot="data.root";

  if(argc!=2){
    printf("Usage  : %s <rootfile>\n",argv[0]);
    printf("Example: %s %s\n",argv[0],iRoot.Data());
     return -1;
  }else{
    iRoot  = argv[1];
  }

  //---------------------------------------------------

  // use the function Open() from TFile to open the ROOT file in read mode
  std::unique_ptr<TFile> f( TFile::Open(iRoot.Data(), "READ") );
  if (!f || f->IsZombie()) {
    printf("Error opening file %s\n",iRoot.Data());
    return -1;
  }

  // Get the Tree object with nvidia-smi samples
  TTree* tree=(TTree*)f->Get("tree");
  if(tree==NULL){
    printf("Tree of data data not found... Exit\n");
    return -1;
  }

//  tree->Show(1);  dump entry 1

  unsigned short absgpu;
  unsigned short node;
  float time;
  unsigned char gpu;
  float watt;
  unsigned short clk;
  unsigned short clk1;
  unsigned char celsius;
  unsigned char celsius1;
  unsigned char usage;
  unsigned char usage1;
  unsigned char pstate;

  float dt;
  char  dgpu;
  float dwatt;
  short dclk;
  short dclk1;
  char dcelsius;
  char dcelsius1;
  char dusage;
  char dusage1;
  char  dpstate;

  tree->SetBranchAddress("absgpu"  ,&absgpu);
  tree->SetBranchAddress("node"    ,&node);
  tree->SetBranchAddress("gpu"     ,&gpu);
  tree->SetBranchAddress("time"    ,&time);
  tree->SetBranchAddress("watt"    ,&watt);
  tree->SetBranchAddress("clk"     ,&clk);
  tree->SetBranchAddress("clk1"    ,&clk1);
  tree->SetBranchAddress("celsius" ,&celsius);
  tree->SetBranchAddress("celsius1",&celsius1);
  tree->SetBranchAddress("usage"   ,&usage);
  tree->SetBranchAddress("usage1"  ,&usage1);
  tree->SetBranchAddress("pstate"  ,&pstate);

  tree->SetBranchAddress("dgpu"     ,&dgpu);
  tree->SetBranchAddress("dt"       ,&dt);
  tree->SetBranchAddress("dwatt"    ,&dwatt);
  tree->SetBranchAddress("dclk"     ,&dclk);
  tree->SetBranchAddress("dclk1"    ,&dclk1);
  tree->SetBranchAddress("dcelsius" ,&dcelsius);
  tree->SetBranchAddress("dcelsius1",&dcelsius1);
  tree->SetBranchAddress("dusage"   ,&dusage);
  tree->SetBranchAddress("dusage1"  ,&dusage1);
  tree->SetBranchAddress("dpstate"  ,&dpstate);

  // floating point (4 bytes)
  TH1F * hPower= new TH1F("hPower" ,"Board Power ; Power [W]; Occurrence [#]",80,0,800);
  // short integer (2 bytes)
  TH1S * hClockCore = new TH1S("hClockCore" , "Clock       ; Clock [MHz]; Occurrence [#]",100,0,2000);
  TH1S * hClockMemo = new TH1S("hClockMemo" , "Clock       ; Clock [MHz]; Occurrence [#]",100,0,2000);
  // char integer (1 byte)
  TH1C * hTempeCore = new TH1C("hTempeCore" , "Temperature ; Temperature [Celsius]; Occurrence [#]",80,20,100);
  TH1C * hTempeMemo = new TH1C("hTempeMemo" , "Temperature ; Temperature [Celsius]; Occurrence [#]",80,20,100);
  TH1C * hUsageCore = new TH1C("hUsageCore" , "Utilization ; Usage [%]; Occurrence [#]",140,-10,130);
  TH1C * hUsageMemo = new TH1C("hUsageMemo" , "Utilization ; Usage [%]; Occurrence [#]",140,-10,130);
/*
  float time;
  float watt;
  float dt;
  float dwatt;

  unsigned short absgpu;
  unsigned short node;
  unsigned short clk;
  unsigned short clk1;
  short dclk;
  short dclk1;

  unsigned char gpu;
  unsigned char celsius;
  unsigned char celsius1;
  unsigned char usage;
  unsigned char usage1;
  unsigned char pstate;
  char  dgpu;
  char dcelsius;
  char dcelsius1;
  char dusage;
  char dusage1;
  char  dpstate;
*/


  // read data and fill histograms

  Int_t nentries = (Int_t)tree->GetEntries();

  printf("Reading data from %s \n",iRoot.Data());
  for (Int_t i=0; i< nentries; i++) {
    if(p)if(i%100000==0) printf("Event %d\n",i);

    tree->GetEntry(i);

    hPower->Fill(watt);
    hClockCore->Fill(clk);
    hClockMemo->Fill(clk1);
    hTempeCore->Fill(celsius);
    hTempeMemo->Fill(celsius1);
    hUsageCore->Fill(usage);
    hUsageMemo->Fill(usage1);

//    if(i<10 || i>(nentries-10) ) p=true; else p=false;



    if(p){
      printf("%d ",absgpu);
      printf("%04d ",node);
      printf("%8.3f ",time);
      printf("%2d "  ,gpu);
      printf("%8.2f ",watt);
      printf("%6d "  ,clk);
      printf("%6d "  ,clk1);
      printf("%6d "  ,celsius);
      printf("%6d "  ,celsius1);
      printf("%6d "  ,usage);
      printf("%6d "  ,usage1);
      printf("%6d "  ,pstate);


      printf("%6.3f ",dt);
      printf("%6d "  ,dgpu);
      printf("%8.2f ",dwatt);
      printf("%6d "  ,dclk);
      printf("%6d "  ,dclk1);
      printf("%6d "  ,dcelsius);
      printf("%6d "  ,dcelsius1);
      printf("%6d "  ,dusage);
      printf("%6d "  ,dusage1);
      printf("%6d "  ,dpstate);

      printf("\n");
    }
  } // end loop on Entries

  std::unique_ptr<TFile> fout( TFile::Open(oRoot.Data(), "RECREATE") );
  fout->WriteObject(hPower, "hPower");
  fout->WriteObject(hClockCore,"hClockCore");
  fout->WriteObject(hClockMemo,"hClockMemo");
  fout->WriteObject(hTempeCore,"hTempeCore");
  fout->WriteObject(hTempeMemo,"hTempeMemo");
  fout->WriteObject(hUsageCore,"hUsageCore");
  fout->WriteObject(hUsageMemo,"hUsageMemo");

  delete hPower;
  delete hClockCore;
  delete hClockMemo;
  delete hTempeCore;
  delete hTempeMemo;
  delete hUsageCore;
  delete hUsageMemo;

  printf("File %s ready\n",oRoot.Data());

  return 0;
}




