#!/bin/bash

rm -f acnode* # E4
rm -f lrdn* # CINECA
rm -f *.x
rm -f *.root
rm -f *.pdf
rm -f all*.txt
rm -f egpu.txt
rm -f enode.txt
rm -f ejob.txt
rm -f offset.t0.txt

exit
