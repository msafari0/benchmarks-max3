#include <stdio.h>
#include <stdlib.h> /*malloc*/
#include <time.h>       /* time_t, struct tm, time, localtime, strftime */


char * getUnixTime_fromDateTime(char * date);


//---------------------------------
int main(int argc,char * argv[]){
//---------------------------------

 char itxt[80];
  sprintf(itxt,"%s","input_filename.txt");

  char otxt[80];
  sprintf(otxt,"%s","output_filename.txt");

  if(argc!=3){
    printf("Usage  : %s <ifilename> <ofilename>\n",argv[0]);
    printf("Example: %s %s %s\n",argv[0],itxt,otxt);
    return -1;
  }else{
    sprintf(itxt,"%s",argv[1]);
    sprintf(otxt,"%s",argv[2]);
  }

  FILE * fin =fopen(itxt,"r");
  FILE * fout =fopen(otxt,"w");


  char istr[80];
  char * ostr;
  while (fgets(istr, sizeof(istr), fin) != NULL) {
    //printf("%s",istr);
    ostr = getUnixTime_fromDateTime(istr);
    fprintf(fout,"%s\n",ostr);
  }


  fclose(fin);
  fclose(fout);
  return 0;
}





//---------------------------------------------
char * getUnixTime_fromDateTime(char * date){
//----------------------------------------------

  time_t converted;
  time_t now;
  struct tm when={0};
  int val[7];

  sscanf(date,"%d/%d/%d %d:%d:%d.%d",val,val+1,val+2,val+3,val+4,val+5,val+6);

  when.tm_year = val[0] - 1900; //Years from 1900
  when.tm_mon  = val[1] - 1; //0-based
  when.tm_mday = val[2]; //1 based
  when.tm_hour = val[3];
  when.tm_min  = val[4];
  when.tm_sec  = val[5];

 //Make sure the daylight savings is same as current timezone.
  now = time(0);
  when.tm_isdst = localtime(&now)->tm_isdst;


  converted = mktime(&when);

//  printf("%s\n",date);
//  printf("%d %d %d %d %d %d %d\n",val[0],val[1],val[2],val[3],val[4],val[5],val[6]);
//  printf("%ld\n",converted);

  char * ret = malloc (sizeof (char) * 80);
  sprintf(ret,"%ld.%03d",converted,val[6]);
  return ret;
}

