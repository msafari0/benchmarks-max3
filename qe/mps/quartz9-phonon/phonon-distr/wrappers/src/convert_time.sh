#!/bin/bash

ITXT="../log/2392589/lrdn0579.nvidiasmi.txt"
OTXT1="nvsmi.txt"
OTXT2="t0.txt"
if [[ $# -ne 3 ]];then
  echo "Usage  : $0 <input nvidia-smi file> <output filename data> <output filename offset>"
  echo "Example: $0 $ITXT $OTXT1 $OXT2"
  exit 666
else
  ITXT=$1
  OTXT1=$2
  OTXT2=$3
fi

LC_ALL="C"

echo "------------------------------------------"
echo "Split Date and Time from other variables"
echo "------------------------------------------"

# split datetime from other variables
awk 'BEGIN { FS = ","}; {printf "%s\n", $2}' $ITXT  > 0
awk 'BEGIN { FS = ","}; {printf "%s %s %s %s %s %s %3.0f %3.0f\n", $1,$3,$4,$5,$6,$7,$8,$9}' $ITXT  > 1
# awk 'BEGIN { FS = ","}; {printf "%s %6.2f %s %s %s %s %3.0f %3.0f\n", $1,$3,$4,$5,$6,$7,$8,$9}' $ITXT  > 1


echo "------------------------------------------"
echo "head file 0 (date and time)"
echo "------------------------------------------"
head 0

echo "------------------------------------------"
echo "head file 1 (other variables)"
echo "------------------------------------------"
head 1


echo "------------------------------------------"
echo "Convert Date and Time to unix timestamp"
echo "------------------------------------------"
echo "./src/convert_date_time_to_unixtime.x 0 2"
./src/convert_date_time_to_unixtime.x 0 2  # it takes few ms!


echo "------------------------------------------"
echo "assign the time offset as the first sample in the file (GPU 0)"
echo "------------------------------------------"

pwd
FILE="2"
OFFSET=$(head -n 1 $FILE)

echo "subtract the offset"
echo ./src/subtract_offset.x $FILE $OFFSET 2.offset
./src/subtract_offset.x $FILE $OFFSET 2.offset

echo "write offset"
echo "echo $OFFSET > $OTXT2"
echo $OFFSET > $OTXT2
date -d "@$OFFSET" >> $OTXT2


echo "merge the two"
echo "paste 2.offset 1 > 3"
paste 2.offset 1 > 3

echo "head 3"
head 3


echo "Write formatted output"
awk '{printf("%1s %8s %s %4s %4s %2s %2s %3s %3s\n",$2,$1,$3,$4,$5,$6,$7,$8,$9,$10,$11)}' 3 > $OTXT1
# awk '{printf("%1s %8s %7.2f %4s %4s %2s %2s %3s %3s\n",$2,$1,$3,$4,$5,$6,$7,$8,$9,$10,$11)}' 3 > $OTXT1

echo "head $OTXT1"
head $OTXT1

echo "Clean up"
rm 0 1 2 3 2.offset

exit

#eof


