#!/bin/bash
if [ $OMPI_COMM_WORLD_LOCAL_RANK -eq 0 ]; then
    export CUDA_MPS_LOG_DIRECTORY=./pipe${OMPI_COMM_WORLD_RANK} ;  export CUDA_MPS_PIPE_DIRECTORY=./pipe${OMPI_COMM_WORLD_RANK} ; nvidia-cuda-mps-control -d
fi
ph.x -nk 8 -ni 4 -i ph.in > out.0_0
