### Grir443 simulation on Leonardo booster

This test shows the scalability of Grir443 with RG parallelization with and without awareness

Number of steps: 6

The folder contains

* the input files, *.UPF and *.in 
* result*.dat the results as generated from JUBE
* *.xml the xml files with parameter concretizations
* 000000 the JUBE directory with application logfiles  

UCX parameters of the default - ucx (MCA v2.1.0, API v3.0.0, Component v4.1.4)

Develop GPU version from QE/7.3.1

Installed with the following software stack

* nvhpc/23.1
* cuda/11.8
* openmpi/4.1.4
* fftw/3.3.10
* openblas/0.3.21

