### Si16L PHONON simulation on Leonardo booster

This test shows the strong scalability of Si16L with image distribution @ q

The folder contains

* the input files, *.upf and *.in ( pwscf step included )
* the output of the scf step ( scf.out )
* result*.dat the results generated from JUBE
* *.xml the xml files with parameter concretizations
* 00000* the JUBE directories with application logfiles  

UCX parameters of the default - ucx (MCA v2.1.0, API v3.0.0, Component v4.1.4)

Release GPU version from QE/7.2

Installed with the following software stack

* nvhpc/23.1
* cuda/11.8
* openmpi/4.1.4
* fftw/3.3.10
* openblas/0.3.21

