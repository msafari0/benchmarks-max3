#!/bin/bash

prefix=p4x2
nimages=$1
END=$(($nimages-1))

cd ./Siout

for i in $(seq 1 $END) ;do
		mkdir _ph${i}
		cp _ph0/${prefix}.xml _ph${i}/.
		cp -r _ph0/${prefix}.phsave _ph${i}/.
		mkdir _ph${i}/${prefix}.save
		cp _ph0/${prefix}.save/*.xml _ph0/${prefix}.save/charge-density.dat _ph${i}/${prefix}.save/.
		cd _ph${i}/${prefix}.save/
		ln -s ../../_ph0/${prefix}.save/wfc* .
		cd ../../
done
