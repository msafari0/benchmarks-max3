### Ausurf


### Platforms

- leonardo booster : Intel Xeon CPU + 4 NVIDIA A100 GPUs (64GB) ; documentation [here](https://wiki.u-gov.it/confluence/display/SCAIUS/UG3.2.1%3A+LEONARDO+Booster+UserGuide)
- marenostrum5 : 2x Intel Sapphire Rapids 8460Y+ at 2.3Ghz and 32c each + 4x Nvidia Hopper GPUs with 64 HBM2 memory ; documentation [here](https://www.bsc.es/supportkc/docs/MareNostrum5/overview)
