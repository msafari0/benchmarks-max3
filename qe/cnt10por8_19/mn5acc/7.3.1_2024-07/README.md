### CNT10POR8 simulation on MareNostrum5 ACC

This test shows the scalability of CNT10POR8 with RG parallelization

The folder contains

* the input files, *.UPF and *.in 
* result*.dat the results as generated from JUBE
* *.xml the xml files with parameter concretizations
* logfiles the JUBE directory with application logfiles  

GPU version from QE/7.3.1

Awareness is active

Max step is 16

Installed with the following software stack

* nvhpc/23.11
* cuda/12.3
* mpi/4.1.5
* mkl/2024.0
* hdf5/1.14.1
