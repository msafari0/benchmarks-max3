import jinja2
import os
import csv
import pandas as pd
import json
import sys
import argparse
import copy
import shutil
import glob
import yaml

def render_index(tpl_path, context):
    path, filename = os.path.split(tpl_path)

    environment = jinja2.Environment(undefined=jinja2.StrictUndefined,
    loader=jinja2.FileSystemLoader(path or '.')) #It means jinja will take templates of environment from file in the introduced path
    # Include url_for_.... in the context
    context['url_for_index'] = 'index.html'
    context['url_for_qe'] = 'qe.html'
    context['url_for_yambo'] = 'yambo.html'
    context['url_for_siesta'] = 'siesta.html'
    context['url_for_fleur'] = 'fleur.html'    
    context['url_for_bigdft'] = 'bigdft.html'

    return environment.get_template(filename).render(context)

def render_qe(tpl_path, context):
    path, filename = os.path.split(tpl_path)

    environment = jinja2.Environment(undefined=jinja2.StrictUndefined,
    loader=jinja2.FileSystemLoader(path or '.')) #
    # Include url_for_... in the context
    context['url_for_index'] = 'index.html'
    context['url_for_qe'] = 'qe.html'
    context['url_for_yambo'] = 'yambo.html'
    context['url_for_siesta'] = 'siesta.html'
    context['url_for_fleur'] = 'fleur.html'    
    context['url_for_bigdft'] = 'bigdft.html'
    context['codeType'] = 'codeType'

    return environment.get_template(filename).render(context)

def render_yambo(tpl_path, context):
    path, filename = os.path.split(tpl_path)

    environment = jinja2.Environment(undefined=jinja2.StrictUndefined,
    loader=jinja2.FileSystemLoader(path or '.')) #
    # Include url_for_index in the context
    context['url_for_index'] = 'index.html'
    context['url_for_qe'] = 'qe.html'
    context['url_for_yambo'] = 'yambo.html'
    context['url_for_siesta'] = 'siesta.html'
    context['url_for_fleur'] = 'fleur.html'    
    context['url_for_bigdft'] = 'bigdft.html'
    context['codeType'] = 'codeType'
    
    return environment.get_template(filename).render(context)

def render_siesta(tpl_path, context):
    path, filename = os.path.split(tpl_path)

    environment = jinja2.Environment(undefined=jinja2.StrictUndefined,
    loader=jinja2.FileSystemLoader(path or '.')) #
    # Include url_for_index in the context
    context['url_for_index'] = 'index.html'
    context['url_for_qe'] = 'qe.html'
    context['url_for_yambo'] = 'yambo.html'
    context['url_for_siesta'] = 'siesta.html'
    context['url_for_fleur'] = 'fleur.html'    
    context['url_for_bigdft'] = 'bigdft.html'
    context['codeType'] = 'codeType'
    
    return environment.get_template(filename).render(context)

def render_fleur(tpl_path, context):
    path, filename = os.path.split(tpl_path)

    environment = jinja2.Environment(undefined=jinja2.StrictUndefined,
    loader=jinja2.FileSystemLoader(path or '.')) #
    # Include url_for_index in the context
    context['url_for_index'] = 'index.html'
    context['url_for_qe'] = 'qe.html'
    context['url_for_yambo'] = 'yambo.html'
    context['url_for_siesta'] = 'siesta.html'
    context['url_for_fleur'] = 'fleur.html'    
    context['url_for_bigdft'] = 'bigdft.html'
    context['codeType'] = 'codeType'

    return environment.get_template(filename).render(context)

def render_bigdft(tpl_path, context):
    path, filename = os.path.split(tpl_path)

    environment = jinja2.Environment(undefined=jinja2.StrictUndefined,
    loader=jinja2.FileSystemLoader(path or '.')) #
    # Include url_for_index in the context
    context['url_for_index'] = 'index.html'
    context['url_for_qe'] = 'qe.html'
    context['url_for_yambo'] = 'yambo.html'
    context['url_for_siesta'] = 'siesta.html'
    context['url_for_fleur'] = 'fleur.html'    
    context['url_for_bigdft'] = 'bigdft.html'
    context['codeType'] = 'codeType'
    
    return environment.get_template(filename).render(context)

def render_template(template_path, entry, entry_info, table_columns, platform, path_of_input, context, plotIndex, codeType, defaultY):
    # Load the template environment
    template_loader = jinja2.FileSystemLoader(searchpath=os.path.dirname(template_path))
    template_env = jinja2.Environment(loader=template_loader, autoescape=True)

    # Include url_for_... in the context
    context['url_for_index'] = 'index.html'
    context['codeType'] = codeType
    context['defaultY'] = defaultY

    def load_csv_data(entry):
        return entry.to_dict(orient='list')

    template_env.filters['load_csv_data'] = load_csv_data

    def to_list(series, format_numbers=False):
        if format_numbers:
            return [float(f'{num:.2f}') for num in series.tolist()]
        else:
            return series.tolist()
    
    template_env.filters['to_list'] = to_list
    context['table_columns'] = table_columns
    context['Ltime_unit'] = entry_info['time_unit']   
    context['system'] = entry_info['system']
    context['platform'] = platform
    context['path_of_input'] = path_of_input 
    context['plotIndex'] = plotIndex
    # Load the template
    template = template_env.get_template(os.path.basename(template_path))

    # Include the dataframe and column_names in the context
    context['dataframe'] = entry
    context['output_path'] = entry_info['output_path']
        
    for column in table_columns:
        context[f'{column}'] = entry[column]
    # Render the template with the provided context
    output_html = template.render(context)

    # Write the rendered HTML to the output file
    with open(entry_info['output_path'], 'w') as output_file:
        output_file.write(output_html)


def gen_index():
    entries = {}
    with open ('index.html', 'w') as f:
        f.write(render_index('.visualisation/templates/index.tmpl', {'entries': entries}))

def gen_qe(loaded_dataframes):
    # Create the context to be used in the template
    context = {}
    entries = []
    # Accessing the dataframes and associated information
    for df_name, df_info in loaded_dataframes.items():
        dataframe = df_info['dataframe']
        output_path = df_info['output_path']
        system_structure_name = df_info['system']
        path_of_input = df_info['path_of_input']
        code = df_info['code']
        machine = df_info['platform']
        version = df_info['version']
        date = df_info['date']
        info1 = df_info['info']
        if code=='qe':
            plotIndex = 0
            codeType = 'Quantum ESPRESSO'
            defaultY = 'walltime'
            table_columns = []
            for i in range(len(dataframe.columns)):
                table_columns.append(dataframe.columns[i])
            # Render the template and save the output HTML
            render_template(".visualisation/templates/plotly_multicomp.tmpl", dataframe, df_info, table_columns, machine, path_of_input, context, plotIndex, codeType, defaultY)

            _, basename = os.path.split(output_path)
            name, _ = os.path.splitext(basename)
            entry = {
                'workload': system_structure_name,
                'info': info1,
                'platform': machine,
                'date': date,
                'version': version,
                'file': output_path,
                'name': name,
                'codeType': codeType,
                # Add more parameters as needed
                }
            entries.append(entry)
    with open ('qe.html', 'w') as f:
        f.write(render_qe('.visualisation/templates/qe.tmpl', {'entries': entries}))
        
def gen_yambo(loaded_dataframes):
    # Create the context to be used in the template
    context = {}
    entries = []
    # Accessing the dataframes and associated information
    for df_name, df_info in loaded_dataframes.items():
        dataframe = df_info['dataframe']
        output_path = df_info['output_path']
        system_structure_name = df_info['system']
        path_of_input = df_info['path_of_input']
        code = df_info['code']
        machine = df_info['platform']
        version = df_info['version']
        date = df_info['date']
        info1 = df_info['info']
        if code=='yambo':
            
            plotIndex = 1
            codeType = 'Yambo'
            defaultY = 'Time-profile'
            table_columns = []
            for i in range(len(dataframe.columns)):
                table_columns.append(dataframe.columns[i])

            # Render the template and save the output HTML    
            render_template(".visualisation/templates/plotly_multicomp.tmpl", dataframe, df_info, table_columns, machine,path_of_input, context, plotIndex, codeType, defaultY)

            _, basename = os.path.split(output_path)
            name, _ = os.path.splitext(basename)
            entry = {
                'workload': system_structure_name,
                'info': info1,
                'platform': machine,
                'date': date,
                'version': version,
                'file': output_path,
                'name': name,
                'codeType': codeType,
                # Add more parameters as needed
                }
            entries.append(entry)
    with open ('yambo.html', 'w') as f:
        f.write(render_yambo('.visualisation/templates/yambo.tmpl', {'entries': entries}))

def gen_siesta(loaded_dataframes):
    # Create the context to be used in the template
    context = {}
    entries = []
    # Accessing the dataframes and associated information
    for df_name, df_info in loaded_dataframes.items():
        dataframe = df_info['dataframe']
        output_path = df_info['output_path']
        system_structure_name = df_info['system']
        path_of_input = df_info['path_of_input']
        code = df_info['code']
        machine = df_info['platform']
        version = df_info['version']
        date = df_info['date']
        info1 = df_info['info']
        if code=='siesta':

            plotIndex = 2
            codeType = 'SIESTA'
            defaultY = 'walltime'
            table_columns = []
            for i in range(len(dataframe.columns)):
                table_columns.append(dataframe.columns[i])

            # Render the template and save the output HTML    
            render_template(".visualisation/templates/plotly_multicomp.tmpl", dataframe, df_info, table_columns, machine,path_of_input, context, plotIndex, codeType, defaultY)

            _, basename = os.path.split(output_path)
            name, _ = os.path.splitext(basename)
            entry = {
                'workload': system_structure_name,
                'info': info1,
                'platform': machine,
                'date': date,
                'version': version,
                'file': output_path,
                'name': name,
                'codeType': codeType,
                # Add more parameters as needed
                }
            entries.append(entry)
    with open ('siesta.html', 'w') as f:
        f.write(render_siesta('.visualisation/templates/siesta.tmpl', {'entries': entries}))
    
def gen_fleur(loaded_dataframes):
    # Create the context to be used in the template
    context = {}
    entries = []
    # Accessing the dataframes and associated information
    for df_name, df_info in loaded_dataframes.items():
        dataframe = df_info['dataframe']
        output_path = df_info['output_path']
        system_structure_name = df_info['system']
        path_of_input = df_info['path_of_input']
        code = df_info['code']
        machine = df_info['platform']
        version = df_info['version']
        date = df_info['date']
        info1 = df_info['info']
        if code=='fleur':

            plotIndex = 3
            codeType = 'FLEUR'
            defaultY = 'tot'
            table_columns = []
            for i in range(len(dataframe.columns)):
                table_columns.append(dataframe.columns[i])

            # Render the template and save the output HTML    
            render_template(".visualisation/templates/plotly_multicomp.tmpl", dataframe, df_info, table_columns, machine,path_of_input, context, plotIndex, codeType, defaultY)

            _, basename = os.path.split(output_path)
            name, _ = os.path.splitext(basename)
            entry = {
                'workload': system_structure_name,
                'info': info1,
                'platform': machine,
                'date': date,
                'version': version,
                'file': output_path,
                'name': name,
                'codeType': codeType,
                # Add more parameters as needed
                }
            entries.append(entry)
    with open ('fleur.html', 'w') as f:
        f.write(render_fleur('.visualisation/templates/fleur.tmpl', {'entries': entries}))
    
def gen_bigdft(loaded_dataframes):
    # Create the context to be used in the template
    context = {}
    entries = []
    # Accessing the dataframes and associated information
    for df_name, df_info in loaded_dataframes.items():
        dataframe = df_info['dataframe']
        output_path = df_info['output_path']
        system_structure_name = df_info['system']
        path_of_input = df_info['path_of_input']
        code = df_info['code']
        machine = df_info['platform']
        version = df_info['version']
        date = df_info['date']
        info1 = df_info['info']
        if code=='bigdft':

            plotIndex = 4
            codeType = 'BigDFT'
            defaultY = 'walltime'
            table_columns = []
            for i in range(len(dataframe.columns)):
                table_columns.append(dataframe.columns[i])

            # Render the template and save the output HTML    
            render_template(".visualisation/templates/plotly_multicomp.tmpl", dataframe, df_info, table_columns, machine,path_of_input, context, plotIndex, codeType, defaultY)

            _, basename = os.path.split(output_path)
            name, _ = os.path.splitext(basename)
            entry = {
                'workload': system_structure_name,
                'info': info1,
                'platform': machine,
                'date': date,
                'version': version,
                'file': output_path,
                'name': name,
                'codeType': codeType,
                # Add more parameters as needed
                }
            entries.append(entry)
    with open ('bigdft.html', 'w') as f:
        f.write(render_bigdft('.visualisation/templates/bigdft.tmpl', {'entries': entries}))
    
    
def count_files(directory, file_extensions=None):
    """Count the number of files and get the files in the provided directory with specific extensions"""
    try:
        # List all items in the directory
        items = os.listdir(directory)

        # Filter out only files with the specified extensions
        if file_extensions:
            items = [item for item in items if os.path.isfile(os.path.join(directory, item)) and any(item.endswith(ext) for ext in file_extensions)]

        # Count the number of files
        file_count = len(items)

        return file_count, items
    except OSError:
        # Handle the case where the directory doesn't exist or is not accessible
        return -1, []

def code_recogniser(file_path, keywords):
    # Extract the folder names from the file path
    folders = file_path.split(os.path.sep)

    # Find the folder with any of the specified keywords
    found_keywords = [folder for folder in folders if any(keyword.lower() in folder.lower() for keyword in keywords)]

    if found_keywords:
        #print(f"Found keywords {found_keywords} in the folder path.")
        return found_keywords[0]
    else:
        print(f"No specified keywords found in the file path.")
        return None

def platform_recogniser(file_path, machines):
    # Extract the folder names from the file path
    folders = file_path.split(os.path.sep)

    # Find the folder with any of the specified keywords
    found_machines = [folder for folder in folders if any(keyword.lower() in folder.lower() for keyword in machines)]

    if found_machines:
        #print(f"Found keywords {found_machines} in the folder path.")
        return found_machines[0]
    else:
        print(f"No specified keywords found in the file path.")
        return None


def recogniser(file_path, position):
    # Extract the folder names from the file path
    folders = file_path.split(os.path.sep)

    # Check if the specified position is valid
    if 0 <= position < len(folders):
        return folders[position]
    else:
        print(f"Invalid position {position}. Please provide a valid position.")
        return None

    
def dataframes_maker(dataframes_list, **kwargs):
    loaded_dataframes = {}
    for num, df_dict in enumerate(dataframes_list, start=1):
        df_name = f'df_{num}'
        dataframe = pd.read_csv(df_dict['filename'], sep=';')
        code = df_dict['code']
        system = df_dict['system']
        time_unit = df_dict['time_unit']
        
        # Ensure missing values are handled as NaN
        #dataframe = dataframe.fillna(value=None)  # Or you can use:   dataframe.fillna(value=0) if desired
        dataframe = dataframe.fillna(value=0)
        # Adjust time unit only for numeric columns, skipping NaN values
        for col in dataframe.columns[3:]:  # Start from the 4th column onwards
            if pd.api.types.is_numeric_dtype(dataframe[col]):
                dataframe[col] = dataframe[col].apply(
                    lambda x: x / (1 if time_unit == 'second' else 60 if time_unit == 'minute' else 3600) if pd.notna(x) else x
                )
            else:
                print(f"Skipping non-numeric column: {col}")

        # Add new information to the dataframe dictionary
        _, basename = os.path.split(df_dict['filename'])
        name, _ = os.path.splitext(basename)
        name_parts = name.split('-')
        
        # Check if there are at least two parts (assuming you want to split into at least two parts)
        if len(name_parts) >= 2:
            kind = name_parts[1]
            dataframe_info = {
            'dataframe': dataframe,
          #  'x_axis': df_dict['x_axis'],
          #  'column_name': df_dict['column_name'],
            'time_unit': df_dict['time_unit'],
          #  'component': df_dict['component'],
            'code': df_dict['code'],
            'system': df_dict['system'],
            'output_path': kwargs.get('output_path', f'./{system}_{kind}.html'),
            'path_of_input': df_dict['path_of_input'],
            'platform': df_dict['platform'],
            'version': df_dict['version'],
            'date': df_dict['date'],
            'info': f'{kind}'
        }
        else:
            kind = "_" 
            dataframe_info = {
            'dataframe': dataframe,
         #   'x_axis': df_dict['x_axis'],
         #   'column_name': df_dict['column_name'],
            'time_unit': df_dict['time_unit'],
         #   'component': df_dict['component'],
            'code': df_dict['code'],
            'system': df_dict['system'],
            'output_path': kwargs.get('output_path', f'./{system}.html'),
            'path_of_input': df_dict['path_of_input'],
            'platform': df_dict['platform'],
            'version': df_dict['version'],
            'date': df_dict['date'],
            'info': f'{kind}'
        }

        loaded_dataframes[df_name] = copy.deepcopy(dataframe_info)

    return loaded_dataframes


def find_input_files(base_path, extensions, subdirs, levels_up=3):
    """
    Search for input files in specified subdirectories, traversing upward and downward.

    Args:
        base_path (str): Starting directory path.
        extensions (list): List of file extensions to look for (e.g., [".in", ".fdf"]).
        subdirs (list): List of subdirectories to search in (e.g., ["work", "inputfiles"]).
        levels_up (int): Number of levels to traverse upward from the base_path.

    Returns:
        list: List of file paths matching the criteria.
    """
    input_files = []
    
    # Traverse upward for the specified number of levels
    for level in range(levels_up):  # Including the base level
        #print(f"Checking level {level}: {base_path}")
        
        # Use os.walk to explore all subdirectories at this level
        for root, dirs, files in os.walk(base_path):
            #print(f"Exploring root: {root}")
            
            # Check for each subdir in the current directory
            for subdir in subdirs:
                search_path = os.path.join(root, subdir)
                #print(f"Looking in subdirectory: {search_path}")

                if os.path.exists(search_path) and os.path.isdir(search_path):
                    for ext in extensions:
                        # Search for files with specific extensions in the subdirectory
                        files_found = glob.glob(f"{search_path}/**/*{ext}", recursive=True)
                        input_files.extend(files_found)
                        #print(f"Files found in {search_path}: {files_found}")
        
        # Move one level up
        base_path = os.path.dirname(base_path)

    return input_files


def parse_parameters_from_file(parameters_file):
    dataframes_list = []
    keywords_to_search = ['yambo', 'qe', 'siesta', 'fleur', 'bigdft']
    machines = ['leonardo-booster', 'lumi', 'marconi', 'mn5acc', 'leonardo-dcpg', 'karolina-cpu', 'marenostrum5-acc', 'deucalion-cpu', 'deucalion-gpu']
    
    with open(parameters_file, 'r') as file:
        parameters = yaml.safe_load(file)

    # Access parameters from the YAML file
    time_unit = parameters['time_unit']
    position_in_path = parameters['position_in_path']
    paths = parameters['paths']
    inputfile_extensions = parameters.get('inputfile_extensions', ['.in', '.fdf', 'xml'])
    #print(inputfile_extensions)
    potential_input_dirs = ['work', 'inputfiles']  # Directories to search for input files

    for file_path in paths:
        file_info = {}
        file_info['filename'] = file_path

        code = code_recogniser(file_path, keywords_to_search)
        machine = platform_recogniser(file_path, machines)

        # Extract code from the folder structure
        version_date = recogniser(file_path, position_in_path + 2)
        version = version_date.split('_')[0]
        date = version_date.split('_')[1]
        system_structure_name = recogniser(file_path, position_in_path)

        # Determine base directory for input files
        base_path, _ = os.path.split(file_path)

        # Search for input files, going 3 levels up
        in_files = find_input_files(base_path, inputfile_extensions, potential_input_dirs, levels_up=3)
        #print(in_files)

        if not in_files:
            # If no input files found, add a placeholder path and a warning message
            print(f"Warning: No input files found for {file_path}.")
            file_info['path_of_input'] = 'Input file not found'
            file_info['warning'] = 'No input file detected. Results may be incomplete.'
        else:
            # Use the first input file found
            source_path = in_files[0]
            destination_path = os.path.join('./', f'{system_structure_name}_{os.path.splitext(source_path)[1]}')
            shutil.copy(source_path, destination_path)
            file_info['path_of_input'] = destination_path
            
        #add additional metadata
        file_info['code'] = code
        file_info['version'] = version
        file_info['date'] = date
        file_info['platform'] = machine
        file_info['system'] = system_structure_name or 'empty'
        file_info['time_unit'] = time_unit

        dataframes_list.append(file_info)

    return dataframes_list



if __name__ == "__main__":
    # Define the data for filling in the template
    page_title = "Benchmarking results"
    print('Welcome Message:')
    print('\tFlask code for visualizing benchmark results of the  MAX  project.')
    print('\tArguments: YAML file which includes time_unit, position of structure name in path, and complete path to each benchmark')
    print('\t Author: Mandana Safari \n')
    if len(sys.argv) < 2:
        print('USAGE: python .visualisation/Chart_plotly.py .visualisation/visualisation_info.yml')
    
    # Modify the argument parsing logic
    parser = argparse.ArgumentParser(description='Parse parameters from a YAML file.')
    parser.add_argument('parameters_file', type=str, help='Path to the parameters file')

   # parser.add_argument('--file', type=str, help='Path to the parameters file')
    
    args = parser.parse_args()
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    dataframes_list = parse_parameters_from_file(args.parameters_file)

    loaded_dataframes = dataframes_maker(dataframes_list)
    print(loaded_dataframes)

    gen_index()
    gen_qe(loaded_dataframes)
    gen_yambo(loaded_dataframes)
    gen_siesta(loaded_dataframes)
    gen_fleur(loaded_dataframes)
    gen_bigdft(loaded_dataframes)


