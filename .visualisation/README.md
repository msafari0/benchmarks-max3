
# CI/CD Web Interface for Benchmark Visualizations

Welcome to the Benchmark Visualizations CI/CD Web Interface! This web application is designed to help users visualize benchmark data for the MAX project. It dynamically generates plots based on data input files and directories, making the analysis process more efficient and interactive.

## Features

- **Interactive Plots**: Users can visualize benchmark results using interactive plots generated with Plotly.
- **Dynamic Dropdowns**: Choose different data columns for the number of Nodes versus the Y axes dynamically.
- **Downloadable Inputs**: Easily access and download input files related to the benchmark data.
- **Support for Multiple Inputs**: The interface accepts multiple file formats and directories for benchmark data.
- **Real-time Visualization**: With CI/CD, changes made to data or input files trigger an automatic regeneration of the plots.

## Constrains
   - This pipline can visualize the benchmarck results (JUBE output) as barchart.
   - The path should have the required information including `./code/structure_name/platform/version_date/result.dat` ( e.g. `./qe/ausurf/leonardo-booster/dev7.2_2023-11/result-noaware.dat`).
   - `VIsualisation_info.yml` file should have your prefered time unit (i.e. second, minute, hour).
   -  The position of the structure name should be indicated in the path directory and be added to `VIsualisation_info.yml` file (e.g. for the path above: `position_in_path: 2`)

## How to Use the Web Interface

1. **Selecting the X and Y Axes**: 
   - The X-axis is fixed and always represents the first column in the input data (number of Nodes).
   - Use the Y-axis dropdown to select different data columns for comparison.

2. **Viewing the Plot**:
   - Once you’ve selected your Y-axis, the plot will automatically update to reflect the new data.
   - The plot is interactive, allowing zooming, panning, and other dynamic features provided by Plotly.

3. **Viewing Input Files**:
   - Below each plot, there is a link to download the input files used for generating the results.
   - The link will open the file in a new tab, or you can right-click to download it directly.

## Instructions for Developers

### Setting Up the Project

1. **Clone the Repository**:
    ```bash
    git clone https://gitlab.com/max-centre/benchmarks-max3.git
    cd https://gitlab.com/max-centre/benchmarks-max3.git
    ```

2. **Install Dependencies**:
   If you use CI/CD pipline, you do not need to be worried about dependencies. All of the nessecary libraries will be installed automatically. 
   In case, one wish to locally run the visualisation python code:
    ```bash
      pip install jinja2 pandas PyYAML
    ```

3. **Configure Your Environment**:
    Ensure the necessary environment variables are set for your CI/CD pipeline and the web application.

4. **Running Locally**:
    In order to run the visualisation pipeline locally, it is important to install the necessary python libraries (Virtual environment is recommended). 
    ```bash
      pip install jinja2 pandas PyYAML
    ```
    You can start the Flask application locally for testing by running:
    ```bash
    python .visualisation/Chart_plotly.py .visualisation/visualisation_info.yml
    ```

### Modifying Plot Configurations

To modify the plot configurations, including adding new datasets, customizing color schemes, or changing the default behavior, you can update the following files:
- **`visualisation_info.yml`**: For adding new benchmark results. 
- **`Chart_plotly.py`**: For backend logic handling input parsing and plot generation.
- **`templates/chart_plotly.tmpl`**: For modifying the HTML structure and JavaScript that powers the interactive features.

### CI/CD Integration

This web interface is integrated into a CI/CD pipeline. Each commit that titled with `[triggerci]` will automatically trigger a regeneration of the HTML plots.

- **CI Pipeline**: Ensure that the CI configuration is properly set up for data input and HTML file generation.
- **Monitoring Logs**: For troubleshooting issues in the pipeline, logs can be accessed via the CI/CD provider’s dashboard.

## File Structure Overview

```
/project-root
│
├── README.md  
:
:             
└── .visualisation              # This directory
     ├── Chart_plotly.py        # Main Flask application
     ├── templates/             # HTML templates for the web interface + Static files (CSS, JavaScript)
     ├── visualisation_info.yml # dataset information (directory, time unit, the position of the structure name in the provided path)
     └── README.md              # This documentation
```

## Troubleshooting

1. **No Plot Displayed**:
   - Ensure that the input file follows the required structure.
   - Check if the CI/CD pipeline is running correctly.

2. **Dropdown Not Populating**:
   - This might be due to a missing or incorrectly formatted input file.
   - Check on the table the existence of non_NaN data. 
   - Check the logs for details.

3. **Performance Issues**:
   - Large datasets may cause a delay in plot rendering. Try using smaller datasets for faster performance.

## License

This project is open-source under the MIT License. Feel free to contribute and improve the tool.

## Contact

For any questions or support, please reach out to the development team at [mandana.safari@max-centre.eu](mailto:mandana.safari@max-centre.eu).
