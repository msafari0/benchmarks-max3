{% extends 'base2.html' %}
{% block content %}
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="Mandana Safari">
		<meta name="description" content="Benchmark visualisation">
		<title> MaX3 benchmarks - {{ system }}</title>
		<link rel="icon" href="https://milligram.github.io/images/icon.png">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.css"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/milligram/1.4.1/milligram.css"/>
   		<script src="https://unpkg.com/gridjs/dist/gridjs.umd.js"></script>
    		<link rel="stylesheet" href="https://unpkg.com/gridjs/dist/theme/mermaid.min.css" />
               <script src="https://cdn.plot.ly/plotly-2.29.1.min.js" charset="utf-8"></script>
<style>
    /* Adjust the width of the chart */
    #myDiv {
        width: 85%; /* Chart takes up 60% of the container */
        height: 450px; /* Adjust the height as needed */
    }

    /* Table takes up 100% of the container */
    #table-container {
        width: 80%;
        height: auto; /* Adjust the height based on your table's content */
    }

    /* Adjust the margins */
    .mt-4 {
        margin-top: 20px; /* Adjust as needed */
    }
</style>

</head>
<body>
 <div class="container">
    <!-- Add a header -->
    <h3><b> {{ system }} on {{ platform }}</b></h3>

    <!-- Add a link to the input file -->
    <a style="margin-bottom: 20px;" href="{{ path_of_input }}" target="_blank"><b> View Input File</b></a>

    <div class="row">
        <div class="col-md-12">
            <!-- Add dropdown menus -->
            <label for="y-dropdown">Select Y axis:</label>
            <select id="y-dropdown">
                <!-- Options will be populated dynamically using JavaScript -->
            </select>

            <!-- Chart container -->
            <div id="chart-container" class="d-flex justify-content-center">
                <div id="myDiv" style="width: 85%;"></div> <!-- Chart is 80% width -->
            </div>
        </div>
    </div>

    <!-- Table container -->
    <div class="row mt-4">
        <div class="col-md-12 d-flex justify-content-center">
            <div id="table-container" style="width: 80%;"></div> <!-- Table is 85% width -->
        </div>
    </div>
</div>

<script>
    const jsonData = {{ dataframe | load_csv_data | tojson | safe }};
  
    // Define the columns to exclude from the y-axis options
    const excludedColumns = ['Nodes', 'Tasks', 'omp', 'Threads/Task']; 


    // Initialize the fixed X-axis to the first column in jsonData
    //const xAxisColumn = Object.keys(jsonData)[0]; // The first column as X-axis
    const xAxisColumn = 'Nodes'; // The first column as X-axis


    //const defaultY = Object.keys(jsonData).slice(-1)[0]; // Last column header
    const defaultY = '{{ defaultY }}';
    const plotIndex = {{ plotIndex }};

	var yValues = Object.keys(jsonData).filter(function(value) {
    return !excludedColumns.includes(value);  // Exclude 'Nodes', 'Tasks', 'omp'
    });

        // Populate dropdown menus
        var yDropdown = document.getElementById('y-dropdown');

        yValues.forEach(function (value) {
            var option = document.createElement('option');
            option.text = value;
            option.value = value;
            yDropdown.appendChild(option);
           if (value === defaultY) {
             option.selected = true; // Set as default if it matches the default value
            }
        });
// Function to generate evenly spaced x-axis tick values
function generateTickValues(data) {
    // Calculate the minimum and maximum values
    const minValue = Math.min(...data);
    const maxValue = Math.max(...data);

    // Calculate the interval between ticks
    const tickInterval = (maxValue - minValue) / (data.length - 1);

    // Generate the tick values array with evenly spaced intervals
    return Array.from({ length: data.length }, (_, i) => minValue + i * tickInterval);
}

// Define an array of colors to use for different plots
const plotColors = ['rgba(230, 25, 75, 0.7)', 'rgba(0, 130, 200, 0.7)', 'rgba(60, 180, 75, 0.7)', 'rgba(245, 130, 48, 0.7)', 'rgba(150, 50, 250, 0.7)'];


// Function to create Plotly trace for a bar chart based on selected x and y values
function makeTrace(x, y, plotIndex) {
    // Get data for the selected x and y values
    const xData = generateTickValues(jsonData[xAxisColumn]); // X-axis fixed to the first column
    const yData = jsonData[y];

    const color = plotColors[plotIndex];

    // Create the trace for the bar chart
    return {
        x: xData.map(String),
        y: yData,
        type: 'bar',
        marker: {
            color: color
        },
 // width: 1
    };
}

// Function to create Plotly plot based on selected x and y values
function makePlot(x, y) {
    // Generate trace based on selected x and y values
    var trace = makeTrace(x, y, plotIndex);
    
    // Plotly configuration
    var layout = {
        //title: plotTitle,
        xaxis: {
            title: 'Nodes',
            tickmode: 'array',
            tickvals: generateTickValues(jsonData[xAxisColumn]),
            ticktext: jsonData[xAxisColumn].map(String),
            tickfont: {
                size: 14
            }
        },
        yaxis: {
            title: y + ' (' + '{{ Ltime_unit }}' + ')',
            tickfont: {
                size: 14
            }
        }
    };

    // Plotly data
    var data = [trace];

    // Plot the chart
    Plotly.newPlot('myDiv', data, layout);
}


        // Event listener for dropdown menu changes
        //document.getElementById('x-dropdown').addEventListener('change', function () {
          //  var selectedX = this.value; // Get selected x value
            //var xAxisTitle = selectedOption.getAttribute('data-xAxis-title'); // Get plotTitle
          //  var selectedY = document.getElementById('y-dropdown').value; // Get selected y value
           //  var plotTitle = selectedOption.getAttribute('data-plot-title'); // Get plotTitle
          //  makePlot(selectedX, selectedY);//, plotTitle, xAxisTitle); // Update plot
        //});

        document.getElementById('y-dropdown').addEventListener('change', function () {
            var selectedY = this.value; // Get selected y value
          //  var plotTitle = selectedOption.getAttribute('data-plot-title'); // Get plotTitle
         //   var selectedX = document.getElementById('x-dropdown').value; // Get selected x value
          //  var xAxisTitle = selectedOption.getAttribute('data-xAxis-title'); // Get plotTitle
            makePlot(xAxisColumn, selectedY);//, plotTitle, xAxisTitle); // Update plot
        });

        // Initialize the plot with default values
        makePlot(xAxisColumn, defaultY);


{% for column in table_columns %}
    const tableD{{ loop.index0 }} = jsonData[{{ column | tojson | safe }}];
{% endfor %}
    const nodes = jsonData[{{ table_columns[0] | tojson | safe }}].map(String);
    
const tableData = [];
for (let i = 0; i < nodes.length; i++) {
    const row = [];

    {% for column in table_columns %}
        row.push(tableD{{ loop.index0 }}[i]);
    {% endfor %}

    tableData.push(row);
}

const columnNames = [{% for column in table_columns %}"{{ column }}", {% endfor %}];

new gridjs.Grid({
    columns: columnNames,
    data: tableData,
    style: {
      table: {
        'font-size': '15px'
      }
    }
}).render(document.getElementById("table-container"));


    
</script>
</body>
</html>
{% endblock %}
