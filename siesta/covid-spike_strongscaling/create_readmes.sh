#!/bin/bash



rdfile="README.md"

dirlist=$(find . -mindepth 3 -maxdepth 3 | sed ':a;N;$!ba;s/\n.\// /g' | sed 's/\.\///')



for dir in $dirlist
do

cd $dir

echo "Covid Spike Protein - Strong Scaling

Tags used in this benchmark:" > $rdfile

grep "<tag>" configuration.xml >> $rdfile

sed -i 's/<tag>/\* /g' $rdfile
sed -i 's/<\/tag>//g' $rdfile

echo "
Files included (apart from JUBE-related files) include:
    * Input fdf files.
    * SIESTA general output file (*.out)
    * Submission script (submit.job)
    * Slurm output files (*.out, *.err), submission stderr, stdout.
    * Timings in *.times and time.json files.
    * Memory allocation info in *.alloc.


SIESTA was compiled by loading the following modules:" >> $rdfile

grep "module load" configuration.xml >> $rdfile

sed -i 's/<parameter name="environ_load" type="string" separator="," duplicate="none" mode="text">//g' $rdfile
sed -i 's/<parameter duplicate="none" mode="text" name="environ_load" separator="," type="string">//g' $rdfile
sed -i 's/<\/parameter>//g' $rdfile
sed -i 's/module load//g' $rdfile

cd ../../..

done
