Covid Spike Protein - Strong Scaling

Tags used in this benchmark:
    * gpu
    * lumi

Files included (apart from JUBE-related files) include:
    * Input fdf files.
    * SIESTA general output file (*.out)
    * Submission script (submit.job)
    * Slurm output files (*.out, *.err), submission stderr, stdout.
    * Timings in *.times and time.json files.
    * Memory allocation info in *.alloc.


SIESTA was compiled by loading the following modules:
       LUMI/24.03 partition/G libreadline/8.2-cpeCray-24.03 PrgEnv-cray;
         cray-hdf5-parallel cray-parallel-netcdf cray-netcdf-hdf5parallel cray-fftw rocm buildtools
