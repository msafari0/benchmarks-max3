
## SIESTA benchmarks
In mid-2024, SIESTA benchmarks were performed in all non-ARM EuroHPC architectures,
relying on the JUBE interface available in [the JUBE4MaX repo](https://gitlab.com/max-centre/JUBE4MaX).

The most relevant results are presented here, with 4 different types of benchmarks:

* Strong Scaling (i.e. same system in the Gamma point, different amount of nodes).
* Weak Scaling (using a number of nodes proportional to the number of basis set functions, taking into account the cubic scaling of diagonalization algorithms).
* Weak Scaling on K-Points (using a number of nodes equal to the amount of K points).
* Weak Scaling on System Size (using a number of nodes proportional to the number of atoms).


## Relevant information on the Workload

The system of choice was the Covid Spike protein in aqueous solution. In its most common case (a single unit cell), the simulation box contains 8783 atoms.
For strong scaling, a SZP basis set was chosen, for a grand total of 57647 orbitals.

As for the three different weak scaling benchmarks:

| nodes | functions for Weak Scaling | K-Points for Weak Scaling on K Points | Atoms for Weak Scaling on System Size |
|-------|----------------------------|---------------------------------------|---------------------------------------|
|   1   |          22292             |                   1                   |                8783                   |
|   2   |          28264             |                   2                   |               17566                   |
|   4   |          35330             |                   4                   |               35132                   |
|   8   |          44584             |                   8                   |               70264 *                 |
|  16   |          56528             |                  16                   |              140528 *                 |
|  32   |          70660             |                  32                   |              281056 *                 |
|  64   |          89168             |                  64                   |              562112 *                 |

For weak scaling in terms of system size, even when using a single-zeta basis set, calculations become unweildy both in terms of memory requirements 
and computational cost; as such, results are not available for more than 4 nodes.
