Covid Spike Protein - Weak Scaling on number of K-Points

Tags used in this benchmark:
    * leonardo

Files included (apart from JUBE-related files) include:
    * Input fdf files.
    * SIESTA general output file (*.out)
    * Submission script (submit.job)
    * Slurm output files (*.out, *.err), submission stderr, stdout.
    * Timings in *.times and time.json files.
    * Memory allocation info in *.alloc.


SIESTA was compiled by loading the following modules:
       intel-oneapi-compilers/2023.2.1 intel-oneapi-mkl/2023.2.0 intel-oneapi-mpi/2021.10.0
         netcdf-fortran/4.6.1--oneapi--2023.2.0
