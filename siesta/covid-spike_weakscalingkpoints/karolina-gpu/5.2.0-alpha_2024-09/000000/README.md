Covid Spike Protein - Weak Scaling on number of K-Points

Tags used in this benchmark:
    * karolina
    * gpu

Files included (apart from JUBE-related files) include:
    * Input fdf files.
    * SIESTA general output file (*.out)
    * Submission script (submit.job)
    * Slurm output files (*.out, *.err), submission stderr, stdout.
    * Timings in *.times and time.json files.
    * Memory allocation info in *.alloc.


SIESTA was compiled by loading the following modules:
       GCC/12.3.0 OpenBLAS LAPACK ScaLAPACK/2.2.0-gompi-2023b-fb libreadline CUDA/12.3.0;
         PnetCDF/1.12.3-gompi-2022a netCDF-Fortran/4.6.1-gompi-2023b FFTW.MPI/3.3.10-gompi-2023b
