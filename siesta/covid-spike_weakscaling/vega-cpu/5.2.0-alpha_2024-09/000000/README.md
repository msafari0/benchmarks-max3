Covid Spike Protein - Weak Scaling on diagonalization workload

Tags used in this benchmark:
    * vega

Files included (apart from JUBE-related files) include:
    * Input fdf files.
    * SIESTA general output file (*.out)
    * Submission script (submit.job)
    * Slurm output files (*.out, *.err), submission stderr, stdout.
    * Timings in *.times and time.json files.
    * Memory allocation info in *.alloc.


SIESTA was compiled by loading the following modules:
       netCDF-Fortran PnetCDF/1.12.3-gompi-2021b libreadline;
         OpenBLAS ScaLAPACK FFTW/3.3.10-GCC-12.3.0;
