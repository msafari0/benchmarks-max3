Covid Spike Protein - Weak Scaling on diagonalization workload

Tags used in this benchmark:
    * meluxina
    * morenodes

Files included (apart from JUBE-related files) include:
    * Input fdf files.
    * SIESTA general output file (*.out)
    * Submission script (submit.job)
    * Slurm output files (*.out, *.err), submission stderr, stdout.
    * Timings in *.times and time.json files.
    * Memory allocation info in *.alloc.


SIESTA was compiled by loading the following modules:
       ScaLAPACK/2.2.0-gompi-2023a-fb netCDF-Fortran/4.6.1-gompi-2023a FFTW/3.3.10-GCC-12.3.0 libreadline/8.2-GCCcore-12.3.0
