Covid Spike Protein - Weak Scaling on diagonalization workload

Tags used in this benchmark:
    * discoverer

Files included (apart from JUBE-related files) include:
    * Input fdf files.
    * SIESTA general output file (*.out)
    * Submission script (submit.job)
    * Slurm output files (*.out, *.err), submission stderr, stdout.
    * Timings in *.times and time.json files.
    * Memory allocation info in *.alloc.


SIESTA was compiled by loading the following modules:
       gcc/latest openblas/0/latest-gcc lapack/latest-gcc scalapack/2/latest-gcc-openmpi ucx/1/latest;
         libfabric/1/latest libaec/1/latest-gcc openmpi/4/gcc/latest zstd/1/latest-gcc zlib/1/latest-gcc fftw/3/latest-gcc-openmpi;
         hdf5/1/1.14/latest-gcc-openmpi netcdf/c/4.9/latest-gcc-openmpi pnetcdf/1/latest-gcc-openmpi netcdf/fortran/4.6/latest-gcc-openmpi
