Covid Spike Protein - Weak Scaling on diagonalization workload

Tags used in this benchmark:
    * morenodes
    * marenostrum

Files included (apart from JUBE-related files) include:
    * Input fdf files.
    * SIESTA general output file (*.out)
    * Submission script (submit.job)
    * Slurm output files (*.out, *.err), submission stderr, stdout.
    * Timings in *.times and time.json files.
    * Memory allocation info in *.alloc.


SIESTA was compiled by loading the following modules:
       intel/2023.2.0 mkl/2023.2.0 ucx/1.15.0 bsc/1.0 ninja/1.11.1 cmake/3.25.1 openmpi/4.1.5
          pnetcdf/1.12.3-openmpi hdf5/1.14.1-2-openmpi  netcdf/c-4.9.2_fortran-4.6.1_cxx4-4.3.1_hdf5-1.14.1-2_pnetcdf-1.12.3-openmpi fftw/3.3.10-openmpi
